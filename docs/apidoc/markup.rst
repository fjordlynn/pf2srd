######
Markup
######


.. automodule:: pf2.sphinx.markup
    :members:
    :undoc-members:
    :show-inheritance:
    :member-order: bysource



Module that might be added to Sphinx-Compendia
==============================================

.. automodule:: pf2.sphinx.compendia
    :members:
    :undoc-members:
    :show-inheritance:
    :member-order: bysource
