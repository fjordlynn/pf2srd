########
Database
########

This module provides an in-memory database for rule entries.

.. autoclass:: pf2.database.database.Database
    :members:
    :undoc-members:
    :show-inheritance:
    :member-order: bysource


.. autoclass:: pf2.database.database.Report
    :members:
    :undoc-members:
    :show-inheritance:
    :member-order: bysource
