######
Models
######

.. autoclass:: pf2.database.Rule
    :members:
    :undoc-members:
    :show-inheritance:
    :member-order: bysource

.. autoclass:: pf2.database.SourceFile
    :members:
    :undoc-members:
    :show-inheritance:
    :member-order: bysource

.. autoclass:: pf2.database.Kind
    :members:
    :undoc-members:
    :show-inheritance:
    :member-order: bysource

.. autoclass:: pf2.database.ReferenceToRule
    :members:
    :undoc-members:
    :show-inheritance:
    :member-order: bysource

.. autoclass:: pf2.database.ScalarReferenceToRule
    :members:
    :undoc-members:
    :show-inheritance:
    :member-order: bysource

.. autoclass:: pf2.database.ComplexReferenceToRule
    :members:
    :undoc-members:
    :show-inheritance:
    :member-order: bysource

All Models
----------

.. automodule:: pf2.database.pf2srd
    :members:
    :undoc-members:
    :show-inheritance:
    :member-order: bysource
