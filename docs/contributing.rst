.. _contributing:

############
Contributing
############

.. todo:: Write a contributor's guide.

Fjordlynn's System Reference Document for Pathfinder Second Edition
(*Fjordlynn PF2SRD*) is an open source project every one can contribute
to.

Fjordlynn is a Sphinx_ documentation project written in RestructuredText_
that use rule entries from YAML_ files.

The entries in a YAML file are for a specific rule ``kind``
(i.e. ``skill``, ``class``, ``spell``). For each kind, Sphinx
directives_ are available for integrating the rule entry information
in the document automatically
(i.e. ``pf2srd:autoskill``, ``pf2srd:autoclass``, ``pf2srd:autospell``).
The general rule guidelines are written as prose in RestructuredText.

.. _Sphinx: https://www.sphinx-doc.org/en/master/
.. _RestructuredText: https://docutils.sourceforge.io/rst.html
.. _directives: https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html
.. _YAML: https://yaml.org/


How to contribute
=================

Contributing code
-----------------

Make sure you have a GitLab account, fork the project, make your changes
and submit a Merge Request. See https://docs.gitlab.com/ee/user/project/merge_requests/
for details.

Contribute feedback
-------------------

Open a ticket with your feedback here: https://gitlab.com/fjordlynn/pf2srd/-/issues/.


Contribution ideas
==================

Data entry
    Entire rulebooks need to be manually entered as RestructuredText and YAML.

    .. todo:: Document the file structure convention for rulebooks and YAML

Entry parsing
    Most code for parsing and structuring rule entries is good enough for now.

Documentation generation
    Each rule kind need some specific code to display its content properly.
    Some kinds are not completed and need improvement.

    .. todo:: Document the API for documentation generation

Styling and website layout
    CSS and JS need improvements to help the website feel polished

    .. todo:: List GUI improvements

Adding more tables and indices
    The code under the tables and indices needs some simplification, and
    tables must be placed and layed out properly.


Building the website
====================

*   Obtain a copy of the source code (using ``git clone`` for instance)

*   Make sure you have a Python interpreter installed

*   (Optional) activate a Python Virtual Environment

    .. code-block:: shell

        # in a POSIX shell
        python -m venv .venv
        . .venv/bin/activate

*   Make sure ``nox`` is installed

    .. code-block:: shell

        pip install nox

*   Build the Website

    .. code-block:: shell

        nox -Rs docs

API Documentation
=================

.. toctree::
    :maxdepth: 1

    apidoc/markup
    apidoc/models
    apidoc/database

File structure
==============

The file structure is currently layed out like so:

.. code-block:: none

    .
    ├── build
    ├── docs
    │   ├── ...
    │   ├── _ext
    │   ├── _static
    │   └── _templates
    ├── raw_data
    │   ├── data
    │   └── fixes.py
    ├── src
    │   └── pf2
    ├── CHANGELOG
    ├── CONTRIBUTING
    ├── LICENSE
    ├── noxfile.py
    ├── OGL.md
    ├── pyproject.toml
    ├── README.rst
    └── test
        ├── conftest.py
        ├── test_database.py
        └── test_entity.py


The ``docs`` folder
-------------------

This folder holds the Sphinx project, the content itself.
Some subfolders have a precise structure:

The ``docs/books`` folder
    holds books, one folder per book, one chapter per file.

The ``docs/_ext`` folder
    Local Sphinx extensions this website uses.

The ``docs/_static`` folder
    Static files for the HTML build (Javascript, CSS, etc).

The ``docs/_templates`` folder
    Jinja templates for customizing the HTML theme.

The ``raw_data`` folder
-----------------------

This folder contains YAML files that defines rule entries.

The ``raw_data/fixes.py`` script
    has a few functions for batch-fixing the rule entries.

The ``src`` folder
------------------

This folder holds Python code for including the raw rule entries in the
documentation. Some notable module include

The ``pf2.database`` module
    manages the YAML file parsing, validation and querying.
    At some point, this should be split in a separate module and only keep
    the models specific to this website.

The ``pf2.sphinx`` module
    manages the rendering of documentation code from loaded rule entries.
    At some point, this should be split in a separate module and only keep
    the models specific to this website.


The root folder
---------------

This folder holds a few configuration files, the most important one being
``noxfile.py`` where we define build and linting commands.

The ``pyproject.toml`` defines a Python package, which should not be published
unless the generic code has been split in a separate code trunk.

Todo
====

Below is a todolist.

.. todolist::
