#########
Rulebooks
#########

This section presents rule books.
Each book presented here as the same content as the original published book
with Product Identity (PI) content removed. Some phrases may be changed
in order to use hyperlinks instead of page numbers.

Indices, glossaries, appendices and other resources may also be rearranged
in order to better use the nature of this medium (a Website).

Core Rules
==========

.. toctree::
    :glob:
    :maxdepth: 2
    :caption: Core Rules

    crb/*

.. pf2srd:autosource:: Pathfinder Core Rulebook (Second Edition)


Bestiary
========

.. toctree::
    :glob:
    :maxdepth: 2
    :caption: Bestiary

    bst1/*

.. pf2srd:autosource:: Pathfinder Bestiary (Second Edition)
