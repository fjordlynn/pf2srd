.. _corerulebook_09_playingthegame:

################
Playing the Game
################

.. todo:: Many missing sections in Chapter 9

    At this point, you have a character and are ready to play Pathfinder! Or maybe you’re
    the GM and you are getting ready to run your first adventure. Either way, this chapter
    provides the full details for the rules outlined in Chapter 1: Introduction. This chapter
    begins by describing the general rules and conventions of how the game is played and then
    presents more in-depth explanations of the rules for each mode of play.

Before diving into how to play Pathfinder, it’s important to
understand the game’s three modes of play, which determine
the pace of your adventure and the specific rules you’ll use
at a given time. Each mode provides a different pace and
presents a different level of risk to your characters. The
Game Master (GM) determines which mode works best
for the story and controls the transition between them.
You’ll likely talk about the modes less formally during
your play session, simply transitioning between exploration
and encounters during the adventure, before heading to a
settlement to achieve something during downtime.

The most intricate of the modes is **encounter mode**. This
is where most of the intense action takes place, and it’s most
often used for combat or other high-stakes situations. The
GM typically switches to encounter mode by calling on
the players to “roll for initiative” to determine the order in
which all the actors take their turns during the encounter.
Time is then divided into a series of rounds, each lasting
roughly 6 seconds in the game world. Each round, player
characters, other creatures, and sometimes even hazards or
events take their turn in initiative order. At the start of a
participant’s turn, they gain the use of a number of actions
(typically 3 in the case of PCs and other creatures) as well as
a special action called a reaction. These actions, and what
you do with them, are how you affect the world within
an encounter. The full rules for playing in encounter mode
start on page 468.

In **exploration mode**, time is more flexible and the play
more free form. In this mode, minutes, hours, or even
days in the game world pass quickly in the real world, as
the characters travel cross country, explore uninhabited
sections of a dungeon, or roleplay during a social
gathering. Often, developments during exploration lead to
encounters, and the GM will switch to that mode of play
until the encounter ends, before returning to exploration
mode. The rules for exploration start on page 479.

The third mode is **downtime**. During downtime, the
characters are at little risk, and the passage of time is
measured in days or longer. This is when you might forge a
magic sword, research a new spell, or prepare for your next
adventure. The rules for downtime are on page 481.

.. sidebar:: Game Conventions

    Pathfinder has many specific rules, but you’ll also want to
    keep these general guidelines in mind when playing.

    The GM Has the Final Say

    If you’re ever uncertain how to apply a rule, the GM decides.
    Of course, Pathfinder is a game, so when adjudicating the
    rules, the GM is encouraged to listen to everyone’s point of
    view and make a decision that is both fair and fun.

    Specific Overrides General

    A core principle of Pathfinder is that specific rules
    override general ones. If two rules conflict, the more
    specific one takes precedence. If there’s still ambiguity,
    the GM determines which rule to use. For example, the
    rules state that when attacking a concealed creature, you
    must attempt a DC 5 flat check to determine if you hit. Flat
    checks don’t benefit from modifiers, bonuses, or penalties,
    but an ability that’s specifically designed to overcome
    concealment might override and alter this. If a rule doesn’t
    specify otherwise, default to the general rules presented
    in this chapter. While some special rules may also state the
    normal rules to provide context, you should always default
    to the normal rules even if effects don’t specifically say to.

    Rounding

    You may need to calculate a fraction of a value, like halving
    damage. Always round down unless otherwise specified.
    For example, if a spell deals 7 damage and a creature takes
    half damage from it, that creature takes 3 damage.

    Multiplying

    When more than one effect would multiply the same
    number, don’t multiply more than once. Instead, combine
    all the multipliers into a single multiplier, with each
    multiple after the first adding 1 less than its value. For
    instance, if one ability doubled the duration of one of your
    spells and another one doubled the duration of the same
    spell, you would triple the duration, not quadruple it.

    Duplicate Effects

    When you’re affected by the same thing multiple times, only
    one instance applies, using the higher level of the effects, or
    the newer effect if the two are the same level. For example,
    if you were using mage armor and then cast it again, you’d
    still benefit from only one casting of that spell. Casting a
    spell again on the same target might get you a better
    duration or effect if it were cast at a higher level the second
    time, but otherwise doing so gives you no advantage.

    Ambiguous Rules

    Sometimes a rule could be interpreted multiple ways.If one
    version is too good to be true, it probably is. If a rule seems
    to have wording with problematic repercussions or doesn’t
    work as intended, work with your group to find a good
    solution, rather than just playing with the rule as printed.

General Rules
=============

Before exploring the specific rules of each mode of play,
you’ll want to understand a number of general rules of
the game. To one degree or another, these rules are used
in every mode of play.

Making Choices
==============

Pathfinder is a game where your choices determine the
story’s direction. Throughout the game, the GM describes
what’s happening in the world and then asks the players,“So
what do you do?” Exactly what you choose to do, and how
the GM responds to those choices, builds a unique story
experience. Every game is different, because you’ll rarely,
if ever, make the same decisions as another group of players.
This is true for the GM as well—two GMs running the exact
same adventure will put different emphasis and flourishes
on the way they present each scenario and encounter.

Often, your choices have no immediate risk or
consequences. If you’re traveling along a forest path and
come across a fork in the trail, the GM will ask, “Which
way do you go?” You might choose to take the right fork
or the left. You could also choose to leave the trail, or just
go back to town. Once your choice is made, the GM tells
you what happens next. Down the line, that choice may
impact what you encounter later in the game, but in many
cases nothing dangerous happens immediately.

But sometimes what happens as a result of your choices
is less than certain. In those cases, you’ll attempt a check.

Checks
======

When success isn’t certain—whether you’re swinging a
sword at a foul beast, attempting to leap across a chasm, or
straining to remember the name of the earl’s second cousin
at a soiree—you’ll attempt a check. Pathfinder has many
types of checks, from skill checks to attack rolls to saving
throws, but they all follow these basic steps.

#.  Roll a d20 and identify the modifiers, bonuses,
    and penalties that apply.
#.  Calculate the result.
#.  Compare the result to the difficulty class (DC).
#.  Determine the degree of success and the effect.

Checks and difficulty classes (DC) both come in many
forms. When you swing your sword at that foul beast,
you’ll make an attack roll against its Armor Class, which
is the DC to hit another creature. If you are leaping across
that chasm, you’ll attempt an Athletics skill check with a
DC based on the distance you are trying to jump. When
calling to mind the name of the earl’s second cousin,
you attempt a check to Recall Knowledge. You might
use either the Society skill or a Lore skill you have
that’s relevant to the task, and the DC depends on how
common the knowledge of the cousin’s name might be,
or how many drinks your character had when they were
introduced to the cousin the night before.

No matter the details, for any check you must roll the
d20 and achieve a result equal to or greater than the DC
to succeed. Each of these steps is explained below.

Step 1: Roll D20 and Identify The Modifiers, Bonuses, and Penalties That Apply
------------------------------------------------------------------------------

Start by rolling your d20. You’ll then identify all the
relevant modifiers, bonuses, and penalties that apply to the
roll. A modifier can be either positive or negative, but a
bonus is always positive, and a penalty is always negative.
The sum of all the modifiers, bonuses, and penalties you
apply to the d20 roll is called your total modifier for that
statistic.

Nearly all checks allow you to add an ability modifier to
the roll. An ability modifier represents your raw capabilities
and is derived from an ability score, as described on page
20. Exactly which ability modifier you use is determined by
what you’re trying to accomplish. Usually a sword swing
applies your Strength modifier, whereas remembering the
name of the earl’s cousin uses your Intelligence modifier.

When attempting a check that involves something you
have some training in, you will also add your **proficiency bonus**.
This bonus depends on your proficiency rank:
untrained, trained, expert, master, or legendary. If you’re
untrained, your bonus is +0—you must rely on raw
talent and any bonuses from the situation. Otherwise, the
bonus equals your character’s level plus a certain amount
depending on your rank. If your proficiency rank is trained,
this bonus is equal to your level + 2, and higher proficiency
ranks further increase the amount you add to your level.

.. list-table:: Proficiency
    :header-rows: 1
    :stub-columns: 1


    *   *   Proficiency Rank
        *   Proficiency Bonus
    *   *   Untrained
        *   0
    *   *   Trained
        *   Your level + 2
    *   *   Expert
        *   Your level + 4
    *   *   Master
        *   Your level + 6
    *   *   Legendary
        *   Your level + 8

There are three other types of bonus that frequently
appear: circumstance bonuses, item bonuses, and status
bonuses. If you have different types of bonus that would
apply to the same roll, you’ll add them all. But if you have
multiple bonuses of the same type, you can use only the
highest bonus on a given roll—in other words, they don’t
“stack.” For instance, if you have both a proficiency bonus
and an item bonus, you add both to your d20 result, but
if you have two item bonuses that could apply to the same
check, you add only the higher of the two.

.. glossary::

    Circumstance bonuses
        typically involve the situation
        you find yourself in when attempting a check. For
        instance, using Raise a Shield with a buckler grants you a
        +1 circumstance bonus to AC. Being behind cover grants
        you a +2 circumstance bonus to AC. If you are both
        behind cover and Raising a Shield, you gain only the +2
        circumstance bonus for cover, since they’re the same type
        and the bonus from cover is higher.

    Item bonuses
        are granted by some item that you
        are wearing or using, either mundane or magical. For
        example, armor gives you an item bonus to AC, while
        expanded alchemist’s tools grant you an item bonus to
        Crafting checks when making alchemical items.

    Status bonuses
        typically come from spells, other
        magical effects, or something applying a helpful, often
        temporary, condition to you. For instance, the 3rd-level
        heroism spell grants a +1 status bonus to attack rolls,
        Perception checks, saving throws, and skill checks. If
        you were under the effect of heroism and someone cast
        the bless spell, which also grants a +1 status bonus on
        attacks, your attack rolls would gain only a +1 status
        bonus, since both spells grant a +1 status bonus to those
        rolls, and you only take the highest status bonus.

    Penalties
        work very much like bonuses. You can have
        circumstance penalties, status penalties, and sometimes
        even item penalties. Like bonuses of the same type, you
        take only the worst all of various penalties of a given
        type. However, you can apply both a bonus and a penalty
        of the same type on a single roll. For example, if you had
        a +1 status bonus from a heroism spell but a –2 status
        penalty from the sickened condition, you’d apply them
        both to your roll—so heroism still helps even though
        you’re feeling unwell.

        Unlike bonuses, penalties can also be **untyped**, in which
        case they won’t be classified as “circumstance,” “item,” or
        “status.” Unlike other penalties, you always add all your
        untyped penalties together rather than simply taking the
        worst one. For instance, when you use attack actions,
        you incur a multiple attack penalty on each attack you
        make on your turn after the first attack, and when you
        attack a target that’s beyond your weapon’s normal
        range increment, you incur a range penalty on the attack.
        Because these are both untyped penalties, if you make
        multiple attacks at a faraway target, you’d apply both the
        multiple attack penalty and the range penalty to your roll.

Once you’ve identified all your various modifiers,
bonuses, and penalties, you move on to the next step.

Step 2: Calculate the Result
----------------------------

This step is simple. Add up all the various modifiers,
bonuses, and penalties you identified in Step 1—this is
your total modifier. Next add that to the number that came
up on your d20 roll. This total is your check result.

Step 3: Compare the Result to the DC
------------------------------------

This step can be simple, or it can create suspense. Sometimes
you’ll know the Difficulty Class (DC) of your check. In these
cases, if your result is equal to or greater than the DC, you
succeed! If your roll anything less than the DC, you fail.

Other times, you might not know the DC right away.
Swimming across a river would require an Athletics check,
but it doesn’t have a specified DC—so how will you know
if you succeed or fail? You call out your result to the GM
and they will let you know if it is a success, failure, or
otherwise. While you might learn the exact DC through
trial and error, DCs sometimes change, so asking the GM
whether a check is successful is the best way to determine
whether or not you have met or exceeded the DC.

Calculating DCs
~~~~~~~~~~~~~~~

Whenever you attempt a check, you compare your result
against a DC. When someone or something else attempts
a check against you, rather than both forces rolling
against one another, the GM (or player, if the opponent is
another PC) compares their result to a fixed DC based on
your relevant statistic. Your DC for a given statistic is 10
+ the total modifier for that statistic.

Step 4: Determine the Degree of Success and Effect
--------------------------------------------------

.. todo:: Add sidebar for calclating results

    .. code-block::

        CALCULATING CHECK RESULTS
        on Number
         the die
         +
         modifier
         Ability
        +
        If you have more than one bonus or penalty of a
        particular type, apply only the highest
        Circumstance Proficiency Status bonus
         bonus
         bonus
         +
         Circumstance Status Item penalty
         penalty
         penalty
        All untyped penalties
        Item bonus
        = Result

Many times, it’s important to determine not only if you
succeed or fail, but also how spectacularly you succeed or
fail. Exceptional results—either good or bad—can cause
you to critically succeed at or critically fail a check.

You critically succeed at a check when a check’s result
meets or exceeds the DC by 10 or more. If the check is an
attack roll, this is sometimes called a critical hit. You can
also critically fail a check. The rules for critical failure—
sometimes called a fumble—are the same as those for a
critical success, but in the other direction: if you fail a
check by 10 or more, that’s a critical failure.

If you rolled a 20 on the die (a “natural 20”), your result
is one degree of success better than it would be by numbers
alone. If you roll a 1 on the d20 (a “natural 1”), your
result is one degree worse. This means that a natural 20
usually results in a critical success and natural 1 usually
results in a critical failure. However, if you were going up
against a very high DC, you might get only a success with
a natural 20, or even a failure if 20 plus your total modifier
is 10 or more below the DC. Likewise, if your modifier for
a statistic is so high that adding it to a 1 from your d20 roll
exceeds the DC by 10 or more, you can succeed even if you
roll a natural 1! If a feat, magic item, spell, or other effect
does not list a critical success or critical failure, treat is as
an ordinary success or failure instead.

Some other abilities can change the degree of success for
rolls you get. When resolving the effect of an ability that
changes your degree of success, always apply the adjustment
from a natural 20 or natural 1 before anything else.

Specific Checks
===============

While most checks follow these basic rules, it’s useful to
know about a few specific types of checks, how they’re
used, and how they differ from one another.

Attack Rolls
------------

When you use a Strike action or any other attack action,
you attempt a check called an attack roll. Attack rolls take
a variety of forms and are often highly variable based on
the weapon you are using for the attack, but there are three
main types: melee attack rolls, ranged attack rolls, and spell
attack rolls. Spell attack rolls work a little bit differently, so
they are explained separately on the next page.

Melee attack rolls use Strength as their ability modifier by
default. If you’re using a weapon or attack with the finesse
trait, then you can use your Dexterity modifier instead.

    Melee attack roll result = d20 roll + Strength
    modifier (or optionally Dexterity modifier for
    a finesse weapon) + proficiency bonus + other
    bonuses + penalties

Ranged attack rolls use Dexterity as their ability modifier.

    Ranged attack roll result = d20 roll + Dexterity
    modifier + proficiency bonus + other bonuses +
    penalties

When attacking with a weapon, whether melee or ranged,
you add your proficiency bonus for the weapon you’re using.
Your class determines your proficiency rank for various
weapons. Sometimes, you’ll have different proficiency ranks
for different weapons. For instance, at 5th level, a fighter
gains the weapon mastery class feature, which grants master
proficiency with the simple and martial weapons of one
weapon group, expert proficiency with advanced weapons
of that group and other simple and martial weapons, and
trained proficiency in all other advanced weapons.

The bonuses you might apply to attack rolls can come
from a variety of sources. Circumstance bonuses can come
from the aid of an ally or a beneficial situation. Status
bonuses are typically granted by spells and other magical
aids. The item bonus to attack rolls comes from magic
weapons—notably, a weapon’s potency rune (page 580).

Penalties to attack rolls come from situations and effects
as well. Circumstance penalties come from risky tactics
or detrimental circumstances, status penalties come from
spells and magic working against you, and item penalties
occur when you use a shoddy item (page 273). When
making attack rolls, two main types of untyped penalties
are likely to apply. The first is the multiple attack penalty,
and the second is the range penalty. The first applies
anytime you make more than one attack action during the
course of your turn, and the other applies only with ranged
or thrown weapons. Both are described below.

Multiple Attack Penalty
~~~~~~~~~~~~~~~~~~~~~~~~~~

The more attacks you make beyond your first in a single
turn, the less accurate you become, represented by the
multiple attack penalty. The second time you use an
attack action during your turn, you take a –5 penalty to
your attack roll. The third time you attack, and on any
subsequent attacks, you take a –10 penalty to your attack
roll. Every check that has the attack trait counts toward
your multiple attack penalty, including Strikes, spell attack
rolls, certain skill actions like Shove, and many others.

Some weapons and abilities reduce multiple attack
penalties, such as agile weapons, which reduce these
penalties to –4 on the second attack or –8 on further attacks.

.. list-table::
    :header-rows: 1
    :stub-columns: 1

    *   *   Attack
        *   Multiple Attack Penalty
        *   Agile
    *   *   First
        *   None
        *   None
    *   *   Second
        *   -5
        *   -4
    *   *   Third or subsequent
        *   -10
        *   -8

Always calculate your multiple attack penalty for the
weapon you’re using on that attack. For example, let’s say
you’re wielding a longsword in one hand and a shortsword
(which has the agile trait) in your other hand, and you are
going to make three Strikes with these weapons during the
course of your turn. The first Strike you make during your
turn has no penalty, no matter what weapon you are using.
The second Strike will take either a –5 penalty if you use
the longsword or a –4 penalty if you use the shortsword.
Just like the second attack, the penalty for your third attack
is based on which weapon you’re using for that particular
Strike. It would be a –10 penalty with the longsword and
a –8 penalty with the shortsword, no matter what weapon
you used for your previous Strikes.

The multiple attack penalty applies only during your
turn, so you don’t have to keep track of it if you can
perform an Attack of Opportunity or a similar reaction
that lets you make a Strike on someone else’s turn.

Range Penalty
~~~~~~~~~~~~~

Ranged and thrown weapons each have a listed range
increment, and attacks with them grow less accurate
against targets farther away (range and range increments
are covered in depth on page 279). As long as your target is
at or within the listed range increment, also called the first
range increment, you take no penalty to the attack roll. If
you’re attacking beyond that range increment, you take a
–2 penalty for each additional increment beyond the first.
You can attempt to attack with a ranged weapon or thrown
weapon up to six range increments away, but the farther
away you are, the harder it is to hit your target.

For example, the range increment of a crossbow is 120
feet. If you are shooting at a target no farther away than
that distance, you take no penalty due to range. If they’re
beyond 120 feet but no more than 240 feet away, you
take a –2 penalty due to range. If they’re beyond 240 feet
but no more than 360 feet away, you take a –4 penalty
due to range, and so on, until you reach the last range
increment: beyond 600 feet but no more than 720 feet
away, where you take a –10 penalty due to range.

Armor Class
~~~~~~~~~~~

.. sidebar:: Striding and striking

    Two of the simplest and most common actions you’ll use in
    combat are Stride and Strike, described in full on page 471.

    Stride is an action that has the move trait and that
    allows you to move a number of feet up to your Speed.
    You’ll often need to Stride multiple times to reach a foe
    who’s far away or to run from danger! Move actions can
    often trigger reactions or free actions. However, unlike
    other actions, a move action can trigger reactions not
    only when you first use the action, but also for every 5
    feet you move during that action, as described on page
    474. The Step action (page 471) lets you move without
    triggering reactions, but only 5 feet.

    Strike is an action that has the attack trait and that
    allows you to attack with a weapon you’re wielding or an
    unarmed attack (such as a fist).

    If you’re using a melee weapon or unarmed attack, your
    target must be within your reach; if you’re attacking with
    a ranged weapon, your target must be within range. Your
    reach is how far you can physically extend a part of your
    body to make an unarmed attack, or the farthest distance
    you can reach with a melee weapon. This is typically 5
    feet, but special weapons and larger creatures have longer
    reaches. Your range is how far away you can attack with
    a ranged weapon or with some types of magical attacks.
    Different weapons and magical attacks have different
    maximum ranges, and ranged weapons get less effective
    as you exceed their range increments.

    Striking multiple times in a turn has diminishing
    returns. The multiple attack penalty (detailed on page
    446) applies to each attack after the first, whether those
    attacks are Strikes, special attacks like the Grapple action
    of the Athletics skill, or spell attack rolls.

Attack rolls are compared to a special difficulty class called
an Armor Class (AC), which measures how hard it is for
your foes to hit you with Strikes and other attack actions.
Just like for any other check and DC, the result of an
attack roll must meet or exceed your AC to be successful,
which allows your foe to deal damage to you.

Armor Class is calculated using the following formula.

    Armor Class = 10 + Dexterity modifier (up to your
    armor’s Dex Cap) + proficiency bonus + armor’s
    item bonus to AC + other bonuses + penalties

Use the proficiency bonus for the category (light,
medium, or heavy) or the specific type of armor you’re
wearing. If you’re not wearing armor, use your proficiency
in unarmored defense.

Armor Class can benefit from bonuses with a variety
of sources, much like attack rolls. Armor itself grants an
item bonus, so other item bonuses usually won’t apply to
your AC, but magic armor can increase the item bonus
granted by your armor.

Penalties to AC come from situations and effects in
much the same way bonuses do. Circumstance penalties
come from unfavorable situations, and status penalties
come from effects that impede your abilities or from
broken armor. You take an item penalty when you wear
shoddy armor (page 273).

Spell Attack Rolls
------------------

If you cast spells, you might be able to make a spell attack
roll. These rolls are usually made when a spell makes an
attack against a creature’s AC.

The ability modifier for a spell attack roll depends on
how you gained access to your spells. If your class grants
you spellcasting, use your key ability modifier. Innate spells
use your Charisma modifier unless the ability that granted
them states otherwise. Focus spells and other sources of
spells specify which ability modifier you use for spell attack
rolls in the ability that granted them. If you have spells from
multiple sources or traditions, you might use different ability
modifiers for spell attack rolls for these different sources of
spells. For example, a dwarf cleric with the Stonewalker
ancestry feat would use her Charisma modifier when casting
meld into stone from that feat, since it’s a divine innate spell,
but she would use her Wisdom modifier when casting heal
and other spells using her cleric divine spellcasting.

Determine the spell attack roll with the following formula.

    Spell attack roll result = d20 roll + ability modifier
    used for spellcasting + proficiency bonus + other
    bonuses + penalties

If you have the ability to cast spells, you’ll have a
proficiency rank for your spell attack rolls, so you’ll
always add a proficiency bonus. Like your ability
modifier, this proficiency rank may vary from one spell
to another if you have spells from multiple sources. Spell
attack rolls can benefit from circumstance bonuses and
status bonuses, though item bonuses to spell attack rolls
are rare. Penalties affect spell attack rolls just like any
other attack roll—including your multiple attack penalty.

Many times, instead of requiring you to make a spell
attack roll, the spells you cast will require those within
the area or targeted by the spell to attempt a saving
throw against your Spell DC to determine how the spell
affects them.

Your spell DC is calculated using the following formula.

    Spell DC = 10 + ability modifier used for
    spellcasting + proficiency bonus + other bonuses +
    penalties

Perception
----------

Perception measures your ability to be aware of your
environment. Every creature has Perception, which works
with and is limited by a creature’s senses (described on
page 464). Whenever you need to attempt a check based
on your awareness, you’ll attempt a Perception check.
Your Perception uses your Wisdom modifier, so you’ll use
the following formula when attempting a Perception check.

    Perception check result = d20 roll + Wisdom
    modifier + proficiency bonus + other bonuses +
    penalties

Nearly all creatures are at least trained in Perception, so
you will almost always add a proficiency bonus to your
Perception modifier. You might add a circumstance
bonus for advantageous situations or
environments, and typically get status bonuses
from spells or other magical effects. Items can
also grant you a bonus to Perception, typically
in a certain situation. For instance, a fine spyglass
grants a +1 item bonus to Perception when attempting
to see something a long distance away. Circumstance
penalties to Perception occur when an environment or
situation (such as fog) hampers your senses, while status
penalties typically come from conditions, spells, and magic
effects that foil the senses. You’ll rarely encounter item
penalties or untyped penalties for Perception.

Many abilities are compared to your Perception DC to
determine whether they succeed. Your Perception DC is
10 + your total Perception modifier.

Perception for Initiative
~~~~~~~~~~~~~~~~~~~~~~~~~

Often, you’ll roll a Perception check to determine your order
in initiative. When you do this, instead of comparing the
result against a DC, everyone in the encounter will compare
their results. The creature with the highest result acts first,
the creature with the second-highest result goes second, and
so on. Sometimes you may be called on to roll a skill check
for initiative instead, but you’ll compare results just as if
you had rolled Perception. The full rules for initiative are
found in the rules for encounter mode on page 468.


Saving Throws
-------------

There are three types of saving throws: Fortitude
saves, Reflex saves, and Will saves. In all
cases, saving throws measure your ability
to shrug off harmful effects in the form
of afflictions, damage, or conditions. You’ll

always add a proficiency bonus to each save. Your class
might give a different proficiency to each save, but you’ll
be trained at minimum. Some circumstances and spells
might give you circumstance or status bonuses to saves,
and you might find resilient armor or other magic items
that give an item bonus.

.. sidebar:: Fortune and misfortune effects

    Fortune and misfortune effects can alter how you roll
    your dice. These abilities might allow you to reroll a
    failed roll, force you to reroll a successful roll, allow you
    to roll twice and use the higher result, or force you to roll
    twice and use the lower result.

    You can never have more than one fortune and more
    than one misfortune effect come into play on a single roll.
    For instance, if an effect lets you roll twice and use the
    higher roll, you can’t then use Halfling Luck (a fortune
    effect) to reroll if you fail. If multiple fortune effects would
    apply, you have to pick which to use. If two misfortune
    effects apply, the GM decides which is worse and applies it.

    If both a fortune effect and a misfortune effect would
    apply to the same roll, the two cancel each other out, and
    you roll normally.

.. pf2srd:savingthrow:: Fortitude

    Fortitude saving throws allow you to reduce the effects
    of abilities and afflictions that can debilitate the body.
    They use your Constitution modifier and are calculated
    as shown in the formula below.

        Fortitude save result = d20 roll + Constitution
        modifier + proficiency bonus + other bonuses +
        penalties

.. pf2srd:savingthrow:: Reflex

    Reflex saving throws measure how well you can
    respond quickly to a situation and how gracefully you
    can avoid effects that have been thrown at you. They use
    your Dexterity modifier and are calculated as shown in
    the formula below.

        Reflex save result = d20 roll + Dexterity modifier +
        proficiency bonus + other bonuses + penalties

.. pf2srd:savingthrow:: Will

    Will saving throws measure how well you can resist
    attacks to your mind and spirit. They use your Wisdom
    modifier and are calculated as shown in the formula below.

        Will save result = d20 roll + Wisdom modifier +
        proficiency bonus + other bonuses + penalties

Sometimes you’ll need to know your DC for a given
saving throw. The DC for a saving throw is 10 + the total
modifier for that saving throw.

Most of the time, when you attempt a saving throw,
you don’t have to use your actions or your reaction.
You don’t even need to be able to act to attempt saving
throws. However, in some special cases you might have
to take an action to attempt a save. For instance, you can
try to recover from the sickened condition by spending an
action to attempt a Fortitude save.

Basic Saving Throws
~~~~~~~~~~~~~~~~~~~

Sometimes you will be called on to attempt a basic saving
throw. This type of saving throw works just like any
other saving throw—the “basic” part refers to the effects.
For a basic save, you’ll attempt the check and determine
whether you critically succeed, succeed, fail, or critically
fail like you would any other saving throw. Then one of
the following outcomes applies based on your degree of
success—no matter what caused the saving throw.

Critical Success
    You take no damage from the spell, hazard, or
    effect that caused you to attempt the save.

Success
    You take half the listed damage from the effect.

Failure
    You take the full damage listed from the effect.

Critical Failure
    You take double the listed damage from the effect.

Skill Checks
------------

Pathfinder has a variety of skills, from Athletics to
Medicine to Occultism. Each grants you a set of related
actions that rely on you rolling a skill check. Each skill
has a key ability score, based on the scope of the skill
in question. For instance, Athletics deals with feats of
physical prowess, like swimming and jumping, so its key
ability score is Strength. Medicine deals with the ability
to diagnose and treat wounds and ailments, so its key
ability score is Wisdom. The key ability score for each
skill is listed in Chapter 4: Skills. No matter which
skill you’re using, you calculate a check for it using the
following formula.

    Skill check result = d20 roll + modifier of the
    skill’s key ability score + proficiency bonus + other
    bonuses + penalties

You’re unlikely to be trained in every skill. When
using a skill in which you’re untrained, your proficiency
bonus is +0; otherwise, it equals your level plus 2 for
trained, or higher once you become expert or better.
The proficiency rank is specific to the skill you’re using.
Aid from another character or some other beneficial
situation may grant you a circumstance bonus. A status
bonus might come from a helpful spell or magical effect.
Sometimes tools related to the skill grant you an item
bonus to your skill checks. Conversely, unfavorable
situations might give you a circumstance penalty to your
skill check, while harmful spells, magic, or conditions
might also impose a status penalty. Using shoddy or
makeshift tools might cause you to take an item penalty.
Sometimes a skill action can be an attack, and in these
cases, the skill check might take a multiple attack
penalty, as described on page 446.

When an ability calls for you to use the DC for a
specific skill, you can calculate it by adding 10 + your
total modifier for that skill.

Notating Total Modifiers
------------------------

When creating your character and adventuring you’ll
record the total modifier for various important checks on
your character sheet. Since many bonuses and penalties
are due to the immediate circumstances, spells, and other
temporary magical effects, you typically won’t apply
them to your notations.

Item bonuses and penalties are often more persistent,
so you will often want to record them ahead of time. For
instance, if you are using a weapon with a +1 weapon
potency rune, you’ll want to add the +1 item bonus to
your notation for your attack rolls with that weapon,
since you will include that bonus every time you attack
with that weapon. But if you have a fine spyglass, you
wouldn’t add its item bonus to your Perception check
notation, since you gain that bonus only if you are using
sight—and the spyglass!—to see long distances.

Special Checks
==============

Some categories of checks follow special rules. The most
notable are flat checks and secret checks.

Flat Checks
-----------

When the chance something will happen or fail to happen
is based purely on chance, you’ll attempt a flat check. A flat
check never includes any modifiers, bonuses, or penalties—
you just roll a d20 and compare the result on the die to
the DC. Only abilities that specifically apply to flat checks
can change the checks’ DCs; most such effects affect only
certain types of flat checks.

If more than one flat check would ever cause or prevent
the same thing, just roll once and use the highest DC. In the
rare circumstance that a flat check has a DC of 1 or lower,
skip rolling; you automatically succeed. Conversely, if one
ever has a DC of 21 or higher, you automatically fail.

Secret Checks
-------------

Sometimes you as the player shouldn’t know the exact
result and effect of a check. In these situations, the rules
(or the GM) will call for a secret check. The secret trait
appears on anything that uses secret checks. This type of
check uses the same formulas you normally would use for
that check, but is rolled by the GM, who doesn’t reveal the
result. Instead, the GM simply describes the information or
effects determined by the check’s result. If you don’t know
a secret check is happening (for instance, if the GM rolls
a secret Fortitude save against a poison that you failed to
notice), you can’t use any fortune or misfortune abilities
(see the sidebar on page 449) on that check, but if a fortune
or misfortune effect would apply automatically, the GM
applies it to the secret check. If you know that the GM is
attempting a secret check—as often happens with Recall
Knowledge or Seek—you can usually activate fortune or
misfortune abilities for that check. Just tell the GM, and
they’ll apply the ability to the check.

The GM can choose to make any check secret, even if
it’s not usually rolled secretly. Conversely, the GM can
let you roll any check yourself, even if that check would
usually be secret. Some groups find it simpler to have players
roll all secret checks and just try to avoid acting on any
out-of-character knowledge, while others enjoy the mystery.

Damage
======

In the midst of combat, you attempt checks to determine
if you can damage your foe with weapons, spells, or
alchemical concoctions. On a successful check, you hit
and deal damage. Damage decreases a creature’s Hit
Points on a 1-to-1 basis (so a creature that takes 6 damage
loses 6 Hit Points). The full rules can be found in the Hit
Points, Healing, and Dying section on page 459.
Damage is sometimes given as a fixed amount, but more
often than not you’ll make a damage roll to determine
how much damage you deal. A damage roll typically uses
a number and type of dice determined by the weapon
or unarmed attack used or the spell cast, and it is often
enhanced by various modifiers, bonuses, and penalties.
Like checks, a damage roll—especially a melee weapon
damage roll—is often modified by a number of modifiers,
penalties, and bonuses. When making a damage roll, you
take the following steps, explained in detail below.

#.  Roll the dice indicated by the weapon, unarmed
    attack, or spell, and apply the modifiers, bonuses,
    and penalties that apply to the result of the roll.

#.  Determine the damage type.

#.  Apply the target’s immunities, weaknesses, and
    resistances to the damage.

#.  If any damage remains, reduce the target’s Hit
    Points by that amount.

Step 1: Roll The Damage Dice and Apply
--------------------------------------

Modifiers, Bonuses, and Penalties
Your weapon, unarmed attack, spell, or sometimes even
a magic item determines what type of dice you roll for
damage, and how many. For instance, if you’re using
a normal longsword, you’ll roll 1d8. If you’re casting
a 3rd-level fireball spell, you’ll roll 6d6. Sometimes,
especially in the case of weapons, you’ll apply modifiers,
bonuses, and penalties to the damage.

When you use melee weapons, unarmed attacks, and
thrown ranged weapons, the most common modifier you’ll
add to damage is your Strength ability modifier. Weapons
with the propulsive trait sometimes add half your Strength
modifier. You typically do not add an ability modifier to
spell damage, damage from most ranged weapons, or
damage from alchemical bombs and similar items.

As with checks, you might add circumstance, status,
or item bonuses to your damage rolls, but if you have
multiple bonuses of the same type, you add only the highest
bonus of that type. Again like checks, you may also apply
circumstance, status, item, and untyped penalties to the
damage roll, and again you apply only the greatest penalty
of a specific type but apply all untyped penalties together.

Use the formulas below.

    Melee damage roll = damage die of weapon or
    unarmed attack + Strength modifier + bonuses +
    penalties

.. pause

    Ranged damage roll = damage die of weapon +
    Strength modifier for thrown weapons + bonuses
    + penalties

.. pause

    Spell (and similar effects) damage roll = damage
    die of the effect + bonuses + penalties

.. pause

Once your damage die is rolled, and you’ve applied any
modifiers, bonuses, and penalties, move on to Step 2. Though
sometimes there are special considerations, described below.

In creasing Damage
~~~~~~~~~~~~~~~~~~

In some cases, you increase the number of dice you roll
when making weapon damage rolls. Magic weapons
etched with the striking rune can add one or more weapon
damage dice to your damage roll. These extra dice are the
same die size as the weapon’s damage die. At certain levels,
most characters gain the ability to deal extra damage from
the weapon specialization class feature.

Persistent Damage
~~~~~~~~~~~~~~~~~

Persistent damage is a condition that causes damage to recur
beyond the original effect. Unlike with normal damage,
when you are subject to persistent damage, you don’t take
it right away. Instead, you take the specified damage at the
end of your turns, after which you attempt a DC 15 flat
check to see if you recover from the persistent damage.
See the Conditions Appendix on pages 618–623 for the
complete rules regarding the persistent damage condition.

Doubling and Halving Damage
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sometimes you’ll need to halve or double an amount of
damage, such as when the outcome of your Strike is a
critical hit, or when you succeed at a basic Reflex save
against a spell. When this happens, you roll the damage
normally, adding all the normal modifiers, bonuses,
and penalties. Then you double or halve the amount as
appropriate (rounding down if you halved it). The GM
might allow you to roll the dice twice and double the
modifiers, bonuses, and penalties instead of doubling
the entire result, but this usually works best for single-
target attacks or spells at low levels when you have a
small number of damage dice to roll. Benefits you gain
specifically from a critical hit, like the flaming weapon
rune’s persistent fire damage or the extra damage die
from the fatal weapon trait, aren’t doubled.

Step 2: Determine The Damage Type
---------------------------------

Once you’ve calculated how much damage you deal,
you’ll need to determine the damage type. There are many
types of damage and sometimes certain types are applied
in different ways. The smack of a club deals bludgeoning
damage. The stab of a spear deals piercing damage. The
staccato crack of a lightning bolt spell deals electricity
damage. Sometimes you might apply precision damage,
dealing more damage for hitting a creature in a vulnerable
spot or when the target is somehow vulnerable. The
damage types are described on page 452.

Damage Types and Traits
~~~~~~~~~~~~~~~~~~~~~~~

When an attack deals a type of damage, the attack action
gains that trait. For example, the Strikes and attack
actions you use wielding a sword when its flaming rune is
active gain the fire trait, since the rune gives the weapon
the ability to deal fire damage.

Step 3: Apply the Target’s Immunities, Weaknesses, and Resistances
------------------------------------------------------------------

Defenses against certain types of damage or effects are
called immunities or resistances, while vulnerabilities are
called weaknesses. Apply immunities first, then weaknesses,
and resistances third. Immunity, weakness, or resistance to
an alignment applies only to damage of that type, not to
damage from an attacking creature of that alignment.

.. sidebar:: Damage types

    Damage has a number of different types and categories,
    which are described below.

    .. pf2srd:autodamagecategory:: Physical

        .. pf2srd:autodamagetype:: Bludgeoning

        .. pf2srd:autodamagetype:: Piercing

        .. pf2srd:autodamagetype:: Slashing

    .. pf2srd:autodamagecategory:: Energy

        .. pf2srd:autodamagetype:: Acid

        .. pf2srd:autodamagetype:: Cold

        .. pf2srd:autodamagetype:: Electricity

        .. pf2srd:autodamagetype:: Fire

        .. pf2srd:autodamagetype:: Sonic

        .. pf2srd:autodamagetype:: Positive

        .. pf2srd:autodamagetype:: Negative

        .. pf2srd:autodamagetype:: Force

    .. pf2srd:autodamagecategory:: Alignment

        .. pf2srd:autodamagetype:: Chaotic

        .. pf2srd:autodamagetype:: Evil

        .. pf2srd:autodamagetype:: Good

        .. pf2srd:autodamagetype:: Lawful

    .. pf2srd:autodamagecategory:: Mental

        .. pf2srd:autodamagetype:: Mental

    .. pf2srd:autodamagecategory:: Poison

        .. pf2srd:autodamagetype:: Poison

    .. pf2srd:autodamagecategory:: Bleed

        .. pf2srd:autodamagetype:: Bleed

    .. pf2srd:autodamagecategory:: Precision

        .. pf2srd:autodamagetype:: Precision

    .. pf2srd:autodamagecategory:: Precious Materials

        .. pf2srd:autodamagetype:: Precious Materials

Immunity
~~~~~~~~

When you have immunity to a specific type of damage, you
ignore all damage of that type. If you have immunity to a
specific condition or type of effect, you can’t be affected by
that condition or any effect of that type. If you have immunity
to effects with a certain trait (such as death effects, poison,
or disease) you are unaffected by any effect with that trait.
Often, an effect can be both a trait and a damage type (this is
especially true in the case of energy damage types). In these
cases, the immunity applies to the entire effect, not just the
damage. You can still be targeted by an ability with an effect
you are immune to; you just don’t apply the effect. However,
some complex effects might have parts that affect you even
if you’re immune to one of the effect’s traits; for instance, a
spell that deals both fire and acid damage can still deal acid
damage to you even if you’re immune to fire.

Immunity to critical hits works a little differently. When
a creature immune to critical hits is critically hit by a Strike
or other attack that deals damage, it takes normal damage
instead of double damage. This does not make it immune
to any other critical success effects of other actions that
have the attack trait (such as Grapple and Shove).

Another exception is immunity to nonlethal attacks. If
you are immune to nonlethal attacks, you are immune to
all damage from attacks with the nonlethal trait, no matter
what other type the damage has. For instance, a stone
golem has immunity to nonlethal attacks. This means
that no matter how hard you hit it with your fist, you’re
not going to damage it—unless your fists don’t have the
nonlethal trait, such as if you’re a monk.

Temporary Immunity
~~~~~~~~~~~~~~~~~~

Some effects grant you immunity to the same effect for a set
amount of time. If an effect grants you temporary immunity,
repeated applications of that effect don’t affect you for as
long as the temporary immunity lasts. Unless the effect says
it applies only to a certain creature’s ability, it doesn’t matter
who created the effect. For example, the blindness spell
says, “The target is temporarily immune to blindness for
1 minute.” If anyone casts blindness on that creature again
before 1 minute passes, the spell has no effect.

Temporary immunity doesn’t prevent or end ongoing
effects of the source of the temporary immunity. For
instance, if an ability makes you frightened and you then gain
temporary immunity to the ability, you don’t immediately
lose the frightened condition due to the immunity you
just gained—you simply don’t become frightened if you’re
targeted by the ability again before the immunity ends.

Weakness
~~~~~~~~

If you have a weakness to a certain type of damage or
damage from a certain source, that type of damage is extra
effective against you. Whenever you would take that type
of damage, increase the damage you take by the value of
the weakness. For instance, if you are dealt 2d6 fire damage
and have weakness 5 to fire, you take 2d6+5 fire damage.

If you have more than one weakness that would apply
to the same instance of damage, use only the highest
applicable weakness value. This usually happens only
when a monster is weak to both a type of physical damage
and the material a weapon is made of.

Resistance
~~~~~~~~~~

If you have resistance to a type of damage, each time
you take that type of damage, you reduce the amount of
damage you take by the listed amount (to a minimum of 0
damage). Resistance can specify combinations of damage
types or other traits. For instance, you might encounter
a monster that’s resistant to non-magical bludgeoning
damage, meaning it would take less damage from
bludgeoning attacks that weren’t magical, but would take
normal damage from your +1 mace (since it’s magical)
or a non-magical spear (since it deals piercing damage).
A resistance also might have an exception. For example,
resistance 10 to physical damage (except silver) would
reduce any physical damage by 10 unless that damage
was dealt by a silver weapon.

If you have more than one type of resistance that
would apply to the same instance of damage, use only the
highest applicable resistance value.

It’s possible to have resistance to all damage. When an
effect deals damage of multiple types and you have resistance
to all damage, apply the resistance to each type of damage
separately. If an attack would deal 7 slashing damage and
4 fire damage, resistance 5 to all damage would reduce the
slashing damage to 2 and negate the fire damage entirely.

Step 4: If Damage Remains, Reduce the Target’s Hit Points
---------------------------------------------------------

After applying the target’s immunities, resistances, and
weaknesses to the damage, whatever damage is left
reduces the target’s Hit Points on a 1-to-1 basis. More
information about Hit Points can be found in the Hit
Points, Healing, and Dying section on page 459.

Nonlethal Attacks
~~~~~~~~~~~~~~~~~

You can make a nonlethal attack in an effort to knock
someone out instead of killing them (see Knocked Out
and Dying on page 459). Weapons with the nonlethal
trait (including fists) do this automatically. You take a
–2 circumstance penalty to the attack roll when you make
a nonlethal attack using a weapon that doesn’t have the
nonlethal trait. You also take this penalty when making a
lethal attack using a nonlethal weapon.

Conditions
==========

The results of various checks might apply conditions to
you or, less often, an item. Conditions change your state of
being in some way. You might be gripped with fear or made
faster by a spell or magic item. One condition represents
what happens when a creature successfully drains your
blood or life essence, while others represent creatures’
attitudes toward you and how they interact with you.
Conditions are persistent; when you’re affected by a
condition, its effects last until the stated duration ends, the
condition is removed, or terms dictated in the condition
cause it to end.
The rules for conditions are summarized
on page 454 and described in full on pages 618–623.

.. todo:: Adjust Condition hyperreferences

Effects
=======

Anything you do in the game has an effect. Many of
these outcomes are easy to adjudicate during the game.
If you tell the GM that you draw your sword, no check
is needed, and the result is that your character is now
holding a sword. Other times, the specific effect requires
more detailed rules governing how your choice is resolved.
Many spells, magic items, and feats create specific effects,
and your character will be subject to effects caused by
monsters, hazards, the environment, and other characters.

While a check might determine the overall impact or
strength of an effect, a check is not always part of creating
an effect. Casting a fly spell on yourself creates an effect
that allows you to soar through the air, but casting the spell
does not require a check. Conversely, using the Intimidate
skill to Demoralize a foe does require a check, and your
result on that check determines the effect’s outcome.

The following general rules are used to understand and
apply effects.

Duration
--------

Most effects are discrete, creating an instantaneous effect
when you let the GM know what actions you are going
to use. Firing a bow, moving to a new space, or taking
something out of your pack all resolve instantly. Other
effects instead last for a certain duration. Once the duration
has elapsed, the effect ends. The rules generally use the
following conventions for durations, though spells have
some special durations detailed on pages 304–305.

For an effect that lasts a number of rounds, the remaining
duration decreases by 1 at the start of each turn of the
creature that created the effect. This is common for beneficial
effects that target you or your allies. Detrimental effects often
last “until the end of the target’s next turn” or “through” a
number of their turns (such as “through the target’s next 3
turns”), which means that the effect’s duration decreases at
the end of the creature’s turn, rather than the start.

Instead of lasting a fixed number of rounds, a duration
might end only when certain conditions are met (or cease to
be true). If so, the effects last until those conditions are met.

Range and Reach
---------------

Actions and other abilities that generate an effect typically
work within a specified range or a reach. Most spells and
abilities list a range—the maximum distance from the
creature or object creating the effect in which the effect
can occur.

Ranged and thrown weapons have a range increment.
Attacks with such weapons work normally up to that
range. Attacks against targets beyond that range take a –2
penalty, which worsens by 2 for every additional multiple
of that range, to a maximum of a –10 penalty after five
additional range increments. Attacks beyond this range
are not possible. For example, if you are using a shortbow,
your attacks take no penalty against a target up to 60 feet
away, a –2 penalty if a target is over 60 and up to 120 feet
away, a –4 if a target is over 120 and up to 180 feet away,
and so on, up to a maximum distance of 360 feet.

Reach is how far you can physically reach with your body
or a weapon. Melee Strikes rely on reach. Your reach also
creates an area around your space where other creatures
could trigger your reactions. Your reach is typically 5 feet,
but weapons with the reach trait can extend this. Larger
creatures can have greater reach; for instance, an ogre has
a 10-foot reach. Unlike with measuring most distances, 10-
foot reach can reach 2 squares diagonally. Reach greater
than 10 feet is measured normally; 20-foot reach can reach
3 squares diagonally, 25-foot reach can reach 4, and so on.

Targets
-------

Some effects require you to choose specific targets.
Targeting can be difficult or impossible if your chosen
creature is undetected by you, if the creature doesn’t match
restrictions on who you can target, or if some other ability
prevents it from being targeted.

Some effects require a target to be willing. Only you
can decide whether your PC is willing, and the GM
decides whether an NPC is willing. Even if you or your
character don’t know what the effect is, such as if your
character is unconscious, you still decide if you’re willing.
Some effects target or require an ally, or otherwise
refer to an ally. This must be someone on your side, often
another PC, but it might be a bystander you are trying to
protect. You are not your own ally. If it isn’t clear, the GM
decides who counts as an ally or an enemy.

Areas
-----

.. todo:: Add areas graphic figure

Some effects occupy an area of a specified shape and size.
An area effect always has a point of origin and extends out
from that point. There are four types of areas: emanations,
bursts, cones, and lines. When you’re playing in encounter
mode and using a grid, areas are measured in the same
way as movement (page 463), but areas’ distances are
never reduced or affected by difficult terrain (page 475) or
lesser cover (page 476). You can use the diagrams below
as common reference templates for areas, rather than
measuring squares each time. Many area effects describe
only the effects on creatures in the area. The GM determines
any effects to the environment and unattended objects.

Burst
~~~~~

A burst effect issues forth in all directions from a single
corner of a square within the range of the effect, spreading
in all directions to a specified radius. For instance, when
you cast fireball, it detonates at the corner of a square
within 500 feet of you and creates a 20-foot burst, meaning
it extends out 20 feet in every direction from the corner of
the square you chose, affecting each creature whose space
(or even one square of its space) is within the burst.

Cone
~~~~

A cone shoots out from you in a quarter circle on the grid.
When you aim a cone, the first square of that cone must
share an edge with your space if you’re aiming orthogonally,
or it must touch a corner of your space if you’re aiming
diagonally. If you’re Large or larger, the first square can
run along the edge of any square of your space. You can’t
aim a cone so that it overlaps your space. The cone extends
out for a number of feet, widening as it goes, as shown in
the Areas diagram. For instance, when a green dragon uses
its breath weapon, it breathes a cone of poisonous gas that
originates at the edge of one square of its space and affects
a quarter-circle area 30 feet on each edge.

If you make a cone originate from someone or
something else, follow these same rules, with the first
square of the cone using an edge or corner of that creature
or object’s space instead of your own.

Emanation
~~~~~~~~~

An emanation issues forth from each side of your space,
extending out to a specified number of feet in all directions.
For instance, the bless spell’s emanation radiates 5 or more
feet outward from the caster. Because the sides of a target’s
space are used as the starting point for the emanation, an
emanation from a Large or larger creature affects a greater
overall area than that of a Medium or smaller creature.

Line
~~~~

A line shoots forth from you in a straight line in a direction
of your choosing. The line affects each creature whose
space it overlaps. Unless a line effect says otherwise, it is
5 feet wide. For example, the lightning bolt spell’s area is
a 60-foot line that’s 5 feet wide.

Line of Effect
--------------

When creating an effect, you usually need an unblocked
path to the target of a spell, the origin point of an effect’s
area, or the place where you create something with a spell
or other ability. This is called a line of effect. You have line
of effect unless a creature is entirely behind a solid physical
barrier. Visibility doesn’t matter for line of effect, nor do
portcullises and other barriers that aren’t totally solid. If
you’re unsure whether a barrier is solid enough, usually
a 1-foot-square gap is enough to maintain a line of effect,
though the GM makes the final call.

In an area effect, creatures or targets must have line of
effect to the point of origin to be affected. If there’s no
line of effect between the origin of the area and the target,
the effect doesn’t apply to that target. For example, if
there’s a solid wall between the origin of a fireball and
a creature that’s within the burst radius, the wall blocks
the effect—that creature is unaffected by the fireball and
doesn’t need to attempt a save against it. Likewise, any
ongoing effects created by an ability with an area cease
to affect anyone who moves outside of the line of effect.

Line of Sight
-------------

Some effects require you to have line of sight to your target.
As long as you can precisely sense the area (as described
in Perception on page 464) and it is not blocked by a
solid barrier (as described in Cover on pages 476–477),
you have line of sight. An area of darkness prevents line
of sight if you don’t have darkvision, but portcullises and
other obstacles that aren’t totally solid do not. If you’re
unsure whether a barrier is solid enough to block line of
sight, usually a 1-foot-square gap is enough to maintain
line of sight, though the GM makes the final call.

Afflictions
===========

Diseases and poisons are types of afflictions, as are curses
and radiation. An affliction can infect a creature for a long
time, progressing through different and often increasingly
debilitating stages. The level of an affliction is the level of
the monster, hazard, or item causing the affliction or, in the
case of a spell, is listed in the affliction entry for that spell.

Format
------

Whether appearing in a spell, as an item, or within a creature’s
stat block, afflictions appear in the following format.

Name and Traits
~~~~~~~~~~~~~~~

The affliction’s name is given first, followed by its traits in
parentheses—including the trait for the type of affliction
(curse, disease, poison, and so forth). If the affliction
needs to have a level specified, it follows the parentheses,
followed by any unusual details, such as restrictions on
removing the conditions imposed by an affliction.

Saving Throw
~~~~~~~~~~~~

When you’re first exposed to the affliction, you must
attempt a saving throw against it. This first attempt to
stave off the affliction is called the initial save. An affliction
usually requires a Fortitude save, but the exact save and its
DC are listed after the name and type of affliction. Spells
that can poison you typically use the caster’s spell DC.

On a successful initial saving throw, you are unaffected
by that exposure to the affliction. You do not need to
attempt further saving throws against it unless you are
exposed to the affliction again.

If you fail the initial saving throw, after the affliction’s
onset period elapses (if applicable), you advance to stage 1
of the affliction and are subjected to the listed effect. On
a critical failure, after its onset period (if applicable), you
advance to stage 2 of the affliction and are subjected to that
effect instead. The stages of an affliction are described below.

Onset
~~~~~

Some afflictions have onset times. For these afflictions, once
you fail your initial save, you don’t gain the effects for the
first stage of the affliction until the onset time has elapsed.
If this entry is absent, you gain the effects for the first stage
(or the second stage on a critical failure) immediately upon
failing the initial saving throw.

Maximum Duration
~~~~~~~~~~~~~~~~

If an affliction lasts only a limited amount of time, it lists a
maximum duration. Once this duration passes, the affliction
ends. Otherwise, the affliction lasts until you succeed at
enough saves to recover, as described in Stages below.

Stages
~~~~~~

An affliction typically has multiple stages, each of which
lists an effect followed by an interval in parentheses.
When you reach a given stage of an affliction, you are
subjected to the effects listed for that stage.

At the end of a stage’s listed interval, you must attempt
a new saving throw. On a success, you reduce the stage
by 1; on a critical success, you reduce the stage by 2. You
are then subjected to the effects of the new stage. If the
affliction’s stage is ever reduced below stage 1, the affliction
ends and you don’t need to attempt further saves unless
you’re exposed to the affliction again.

On a failure, the stage increases by 1; on a critical failure,
the stage increases by 2. You are then subjected to the
effects listed for the new stage. If a failure or critical failure
would increase the stage beyond the highest listed stage, the
affliction instead repeats the effects of the highest stage.

Conditions from Afflictions
---------------------------

.. sidebar:: Affliction example

    To see how a poison works, let’s look at the arsenic
    alchemical item (page 550). The item notes that you can’t
    reduce your sickened condition while affected by arsenic,
    and has the following text for how the affliction works.

        Saving Throw DC 18 Fortitude; Onset 10 minutes;
        Maximum Duration 5 minutes; Stage 1 1d4 poison
        damage and sickened 1 (1 minute); Stage 2 1d6 poison
        damage and sickened 2 (1 minute); Stage 3 2d6 poison
        damage and sickened 3 (1 minute)

    For example, if you drank a glass of wine laced with
    arsenic, you would attempt an initial Fortitude save
    against the listed DC of 18. If you fail, you advance to
    stage 1. Because of the onset time, nothing happens
    for 10 minutes, but once this time passes, you take
    1d4 poison damage and become sickened 1. As noted,
    you’re unable to reduce the sickened condition you
    gain from arsenic. The interval of stage 1 is 1 minute (as
    shown in parentheses), so you attempt a new save after
    1 minute passes. If you succeed, you reduce the stage
    by 1, recovering from the poison. If you fail again, the
    stage increases by 1 to stage 2, and you take 1d6 poison
    damage and become sickened 2.

    If your initial save against the arsenic was a critical
    failure, you would advance directly to stage 2. After the
    10-minute onset time, you would take 1d6 poison damage
    and become sickened 2. Succeeding at your second save
    would reduce the stage t by 1 to stage 1, and you’d take
    only 1d4 poison damage. Failing the second save would
    increase by 1 again to stage 3.

    If you reach stage 3 of the poison, either by failing
    while at stage 2 or critically failing while at stage 1, you’d
    take 2d6 poison damage and be sickened 3. If you failed
    or critically failed your saving throw while at stage 3, you
    would repeat the effects of stage 3.

    Since the poison has a maximum duration of 5 minutes,
    you recover from it once the 5 minutes pass, no matter
    which stage you’re at.

An affliction might give you conditions with a longer or
shorter duration than the affliction. For instance, if an
affliction causes you to be drained but has a maximum
duration of 5 minutes, you remain drained even after the
affliction ends, as is normal for the drained condition. Or,
you might succeed at the flat check to remove persistent
damage you took from an ongoing affliction, but you would
still need to attempt saves to remove the affliction itself, and
failing one might give you new persistent damage.

Multiple Exposures
------------------

Multiple exposures to the same curse or disease currently
affecting you have no effect. For a poison, however, failing
the initial saving throw against a new exposure increases
the stage by 1 (or by 2 if you critically fail) without
affecting the maximum duration. This is true even if you’re
within the poison’s onset period, though it doesn’t change
the onset length.

Virulent Afflictions
--------------------

Afflictions with the virulent trait are harder to remove.
You must succeed at two consecutive saves to reduce a
virulent affliction’s stage by 1. A critical success reduces a
virulent affliction’s stage by only 1 instead of by 2.

Counteracting
=============

Some effects try to counteract spells, afflictions, conditions, or
other effects. Counteract checks compare the power of two
forces and determine which defeats the other. Successfully
counteracting an effect ends it unless noted otherwise.

When attempting a counteract check, add the relevant
skill modifier or other appropriate modifier to your check
against the target’s DC. If you’re counteracting an affliction,
the DC is in the affliction’s stat block. If it’s a spell, use the
caster’s DC. The GM can also calculate a DC based on the
target effect’s level. For spells, the counteract check modifier
is your spellcasting ability modifier plus your spellcasting
proficiency bonus, plus any bonuses and penalties that
specifically apply to counteract checks. What you can
counteract depends on the check result and the target’s
level. If an effect is a spell, its level is the counteract level.
Otherwise, halve its level and round up to determine its
counteract level. If an effect’s level is unclear and it came
from a creature, halve and round up the creature’s level.

Critical Success
    Counteract the target if its counteract level is no
    more than 3 levels higher than your effect’s counteract level.

Success
    Counteract the target if its counteract level is no
    more than 1 level higher than your effect’s counteract level.

Failure
    Counteract the target if its counteract level is lower
    than your effect’s counteract level.

Critical Failure
    You fail to counteract the target.

Hit Points, Healing, and Dying
==============================

All creatures and objects have Hit Points (HP). Your
maximum Hit Point value represents your health,
wherewithal, and heroic drive when you are in good
health and rested. Your maximum Hit Points include
the Hit Points you gain at 1st level from your ancestry
and class, those you gain at higher levels from your class,
and any you gain from other sources (like the Toughness
general feat). When you take damage, you reduce your
current Hit Points by a number equal to the damage dealt.

Some spells, items, and other effects, as well as simply
resting, can heal living or undead creatures. When you
are healed, you regain Hit Points equal to the amount
healed, up to your maximum Hit Points.

Knocked Out and Dying
---------------------

Creatures cannot be reduced to fewer than 0 Hit Points.
When most creatures reach 0 Hit Points, they die and are
removed from play unless the attack was nonlethal, in which
case they are instead knocked out for a significant amount
of time (usually 1 minute or more). When undead and
construct creatures reach 0 Hit Points, they are destroyed.

Player characters, their companions, and other
significant characters and creatures don’t automatically
die when they reach 0 Hit Points. Instead, they are
knocked out and are at risk of death. At the GM’s
discretion, villains, powerful monsters, special NPCs, and
enemies with special abilities that are likely to bring them
back to the fight (like ferocity, regeneration, or healing
magic) can use these rules as well.

As a player character, when you are reduced to 0 Hit
Points, you’re knocked out with the following effects:

*   You immediately move your initiative position to
    directly before the creature or effect that reduced
    you to 0 HP.

*   You gain the dying 1 condition. If the effect that
    knocked you out was a critical success from the
    attacker or the result of your critical failure, you
    gain the dying 2 condition instead. If you have the
    wounded condition (page 460), increase your dying
    value by an amount equal to your wounded value.
    If the damage was dealt by a nonlethal attack or
    nonlethal effect, you don’t gain the dying condition;
    you are instead unconscious with 0 Hit Points.

Taking Damage while Dying
~~~~~~~~~~~~~~~~~~~~~~~~~

If you take damage while you already have the dying
condition, increase your dying condition value by 1, or by
2 if the damage came from an attacker’s critical hit or your
own critical failure. If you have the wounded condition,
remember to add the value of your wounded condition to
your dying value.

Recovery Checks
---------------

When you’re dying, at the start of each of your turns, you
must attempt a flat check with a DC equal to 10 + your
current dying value to see if you get better or worse. This
is called a recovery check. The effects of this check are
as follows.

Critical Success
    Your dying value is reduced by 2.

Success
    Your dying value is reduced by 1.

Failure
    Your dying value increases by 1.

Critical Failure
    Your dying value increases by 2.

Conditions Related to Death and Dying
-------------------------------------

To understand the rules for getting knocked out and
how dying works in the game, you’ll need some more
information on the conditions used in those rules.
Presented below are the rules for the dying, unconscious,
wounded, and doomed conditions.

.. pf2srd:autocondition:: Dying

.. pf2srd:autocondition:: Unconscious

.. pf2srd:autocondition:: Wounded

.. pf2srd:autocondition:: Doomed

Death
-----

After you die, you can’t act or be affected by spells
that target creatures (unless they specifically target
dead creatures), and for all other purposes you
are an object. When you die, you are reduced
to 0 Hit Points if you had a different amount,
and you can’t be brought above 0 Hit Points
as long as you remain dead. Some magic can
bring creatures back to life, such as the
*resurrect* ritual or the *raise dead* spell.

Heroic Recovery
---------------

If you have at least 1
Hero Point (page 467),
you can spend all of your
remaining Hero Points at the
start of your turn or when your dying
value would increase in order to return to 1
Hit Point, no matter how close to death you
are. You lose the dying condition and
become conscious. You do not
gain the wounded condition (or
increase its value) when you
perform a heroic recovery.

Death Effects and Instant Death
-------------------------------

Some spells and abilities can kill you immediately or bring
you closer to death without needing to reduce you to
0 Hit Points first. These abilities have the death trait and
usually involve negative energy, the antithesis of life. If you
are reduced to 0 Hit Points by a death effect, you are slain
instantly without needing to reach dying 4. If an effect states
it kills you outright, you die without having to reach
dying 4 and without being reduced to 0 Hit Points.

Massive Damage
--------------

You die instantly if you ever take damage equal to or
greater than double your maximum Hit Points in one blow.

Temporary Hit Points
--------------------

Some spells or abilities give you temporary Hit Points.
Track these separately from your current and maximum
Hit Points; when you take damage, reduce your temporary
Hit Points first. Most temporary Hit Points last for a
limited duration. You can’t regain lost temporary Hit
Points through healing, but you can gain more via other
abilities. You can have temporary Hit Points from only one
source at a time. If you gain temporary Hit Points when
you already have some, choose whether to keep the amount
you already have and their corresponding duration or to
gain the new temporary Hit Points and their duration.

Items and Hit Points
--------------------

Items have Hit Points like creatures, but the rules for
damaging them are different (page 272). An item has a
Hardness statistic that reduces damage the item takes by that
amount. The item then takes any damage left over. If an item
is reduced to 0 HP, it’s destroyed. An item also has a Broken
Threshold. If its HP are reduced to this amount or lower,
it’s broken, meaning it can’t be used for its normal function
and it doesn’t grant bonuses. Damaging an unattended item
usually requires attacking it directly, and can be difficult due
to that item’s Hardness and immunities. You usually can’t
attack an attended object (one on a creature’s person).

Actions
=======

You affect the world around you primarily by using
actions, which produce effects. Actions are most closely
measured and restricted during the encounter mode of
play, but even when it isn’t important for you to keep strict
track of actions, they remain the way in which you interact
with the game world. There are four types of actions: single
actions, activities, reactions, and free actions.

.. pf2srd:autoactioncost:: Single Action

Activities usually take longer and require using multiple
actions, which must be spent in succession. Stride is a single
action, but Sudden Charge is an activity in which you use
both the Stride and Strike actions to generate its effect.

.. pf2srd:autoactioncost:: Reaction

.. pf2srd:autoactioncost:: Free Action

Activities
----------

An activity typically involves using multiple actions to
create an effect greater than you can produce with a
single action, or combining multiple single actions to
produce an effect that’s different from merely the sum of
those actions. In some cases, usually when spellcasting,
an activity can consist of only 1 action, 1 reaction, or
even 1 free action.

An activity might cause you to use specific actions
within it. You don’t have to spend additional actions to
perform them—they’re already factored into the activity’s
required actions. (See Subordinate Actions on page 462.)

You have to spend all the actions of an activity at
once to gain its effects. In an encounter, this means you
must complete it during your turn. If an activity gets
interrupted or disrupted in an encounter (page 462), you
lose all the actions you committed to it.

Exploration and Downtime Activities
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Basic Actions
=============

.. pf2srd:autoactivity:: Escape

.. pf2srd:autoactivity:: Stride

.. pf2srd:autoactivity:: Fly
