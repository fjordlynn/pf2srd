.. _corerulebook_03_classes:

#######
Classes
#######

.. pf2srd:autoclass:: Alchemist

.. pf2srd:autoclass:: Barbarian

.. pf2srd:autoclass:: Bard

.. pf2srd:autoclass:: Champion

.. pf2srd:autoclass:: Cleric

.. pf2srd:autoclass:: Druid

.. pf2srd:autoclass:: Fighter

.. pf2srd:autoclass:: Monk

.. pf2srd:autoclass:: Ranger

.. pf2srd:autoclass:: Rogue

.. pf2srd:autoclass:: Sorcerer

.. pf2srd:autoclass:: Wizard
