"""
Command line interface.

This module provides argument parsing, shell completion and other CLI
goodies.
"""

from __future__ import annotations

import logging.config
from pathlib import Path
from typing import TYPE_CHECKING

import click

if TYPE_CHECKING:
    from typing import Any


log = logging.getLogger()


PROJECT = Path(__file__).parent.parent.parent


@click.group(invoke_without_command=True)
@click.version_option(version="0.1")
@click.option(
    "-v",
    "--verbose",
    count=True,
    help="Increase log level. '-v' will output INFO messages. "
    "More than -vv is useless.",
)
@click.option(
    "-q",
    "--quiet",
    count=True,
    help="Reduce log level. '-q' will suppress WARNING messages. "
    "More than -qq is useless.",
)
@click.option(
    "-l",
    "--log-file",
    help="Reduce log level. '-q' will suppress WARNING messages. "
    "More than -qq is useless.",
)
@click.pass_context
def main(ctx: click.Context, verbose: int, quiet: int, log_file: str) -> None:
    """
    outil, the One UTIL package to rule them all.
    """
    verbosity: int = int(logging.INFO / 10) + verbose - quiet
    logging.config.dictConfig(
        logging_configuration(verbosity, Path(log_file) if log_file else None)
    )

    if ctx.invoked_subcommand is None:
        click.echo("not really")


@main.command()
@click.argument("directory", metavar="DIRECTORY", nargs=1)
def checkdb(directory: str) -> None:
    from pf2.database.database import Database
    from pf2.yaml import make_yaml

    yaml_load = make_yaml()
    yaml_dumps = make_yaml()

    data_directory = Path(directory)

    database = Database([], yaml_load=yaml_load.load, yaml_dumps=yaml_dumps.dumps)

    for filepath in data_directory.rglob("*.yaml"):
        if (
            filepath.name
            in (
                "activity.yaml",
                "feat.yaml",
                "spell.yaml",
            )
            or "monster" in filepath.parts
        ):
            continue
        database.load_from_file(filepath)

    report = database.check()
    click.echo(report.yaml())


def logging_configuration(
    verbosity: int, logfile: Path | None = None
) -> dict[str, Any]:
    """
    Build logging configuration based on a verbosity level.

    Args:
        verbosity:
            Verbosity 0 is means "CRITICAL", and each increments move
            toward "DEBUG".
        logfile:
            Optional log file.
    """
    level = max(logging.CRITICAL - logging.DEBUG * verbosity, logging.DEBUG)

    formatters = {
        "standard": {"format": "%(levelname)8s: %(message)s"},
        "debug": {"format": "%(levelname)8s %(name)-24s> %(message)s"},
    }

    active_formatter = "debug" if level < logging.DEBUG else "standard"

    handlers = {
        "console": {
            "level": level,
            "formatter": active_formatter,
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stderr",  # Default is stderr
        },
    }

    if logfile:
        handlers["file"] = {
            "level": level,
            "formatter": active_formatter,
            "class": "logging.FileHandler",
            "filename": str(logfile),
        }

    loggers = {
        "": {
            # root logger
            "handlers": handlers.keys(),
            "level": level,
            "propagate": True,
        }
    }

    return {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": formatters,
        "handlers": handlers,
        "loggers": loggers,
    }


if __name__ == "__main__":
    main(prog_name="pf2")
