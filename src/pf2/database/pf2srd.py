from __future__ import annotations

import logging
from datetime import date
from typing import Annotated, ClassVar, TypedDict

from pydantic import AnyHttpUrl, Field, root_validator, validator

from pf2.database import BaseModel, ComplexReferenceToRule, Rule, ScalarReferenceToRule

log = logging.getLogger(__name__)


class Publisher(Rule):
    website: Annotated[
        AnyHttpUrl,
        Field(
            ...,
            description="URL to this publisher's website.",
        ),
    ]


class ReferenceToPublisher(ScalarReferenceToRule[Publisher], target=Publisher):
    ...


class Source(Rule):
    short_name: Annotated[
        str,
        Field(
            ...,
            description="A short vernacular name for this source.",
        ),
    ]

    title: Annotated[
        str,
        Field(
            ...,
            description="The official full name for this source.",
        ),
    ]

    abbreviation: Annotated[
        str,
        Field(
            ...,
            description="The official full name for this source.",
        ),
    ]

    isbn: Annotated[
        str | None,
        Field(
            description=(
                "The International Standard Book Number (ISBN, ISO-2108) "
                "for this source."
            ),
        ),
    ] = None

    publisher: Annotated[
        ReferenceToPublisher,
        Field(
            ...,
            description="Rule who published this source.",
        ),
    ]

    pzocode: Annotated[
        str,
        Field(
            ...,
            description="Internal rule book identifier used by Paizo.",
        ),
    ]

    release_date: Annotated[
        date,
        Field(
            ...,
            description="Date when this source was released to the public.",
        ),
    ]

    copyright_block: Annotated[
        str,
        Field(
            description="Copyright notice for this source.",
            alias="ogl_copyright_block",
        ),
    ] = ""

    _additional_name_fields: ClassVar[tuple[str, ...]] = (
        "name",
        "short_name",
        "abbreviation",
        "pzocode",
    )


class ReferenceToSource(ComplexReferenceToRule[Source], target=Source):
    page_start: Annotated[
        int,
        Field(
            description=(
                "Page in the source where the referenced entity is documented from."
            )
        ),
    ]
    page_stop: Annotated[
        int,
        Field(
            description=(
                "Page in the source where the referenced entity is documented to."
            )
        ),
    ]

    @validator("page_stop", pre=True, always=True)
    def _page_stop_default_to_page_start(cls, v, *, values, **kwargs):  # type: ignore[no-untyped-def] # noqa: N805, ANN001, ANN202, ARG002, ANN003
        return v or values["page_start"]


class WithSources:
    sources: Annotated[list[ReferenceToSource], Field(default_factory=list)]


class TraitType(Rule):
    ...


class ReferenceToTraitType(ScalarReferenceToRule[TraitType], target=TraitType):
    ...


class Trait(Rule):
    traittype: ReferenceToTraitType


class ReferenceToTrait(ScalarReferenceToRule[Trait], target=Trait):
    ...


class WithTraits:
    traits: Annotated[list[ReferenceToTrait], Field(default_factory=list)]


class Alignment(Rule, WithSources):
    abbreviation: Annotated[
        str,
        Field(
            ...,
            description=("Very short representation for this alignment."),
        ),
    ]


class ReferenceToAlignment(ScalarReferenceToRule[Alignment], target=Alignment):
    ...


class AbilityScore(Rule, WithSources):
    short_name: str


class ReferenceToAbilityScore(ScalarReferenceToRule[AbilityScore], target=AbilityScore):
    ...


class ActionCategory(Rule, WithSources):
    ...


class ReferenceToActionCategory(
    ScalarReferenceToRule[ActionCategory], target=ActionCategory
):
    ...


class ActionCost(Rule):
    abbreviation: Annotated[
        str,
        Field(
            ...,
            description=("Very short representation for this action cost."),
        ),
    ]


class ReferenceToActionCost(ScalarReferenceToRule[ActionCost], target=ActionCost):
    ...


class EffectMixin:
    range: Annotated[str | None, Field(description="")] = None  # noqa: A003
    area: Annotated[str | None, Field(description="")] = None
    target: Annotated[str | None, Field(description="")] = None
    duration: Annotated[str | None, Field(description="")] = None

    critical_success: Annotated[str | None, Field(description="")] = None
    success: Annotated[str | None, Field(description="")] = None
    failure: Annotated[str | None, Field(description="")] = None
    critical_failure: Annotated[str | None, Field(description="")] = None

    @validator("duration", pre=True)
    def _coerce_duration(cls, v):  # type: ignore[no-untyped-def] # noqa: N805, ANN001, ANN202
        return v or "instantaneous"


class Effect(Rule, EffectMixin, WithSources, WithTraits):
    ...


class ReferenceToEffect(ScalarReferenceToRule[Effect], target=Effect):
    ...


class Bulk(Rule, WithSources):
    symbol: Annotated[
        str,
        Field(
            ...,
            description="One caracter representation of this bulk.",
        ),
    ]
    numerical: Annotated[
        float,
        Field(
            ...,
            description="Numerical equivalent for this bulk.",
        ),
    ]


class ReferenceToBulk(ScalarReferenceToRule[Bulk], target=Bulk):
    ...


class ItemCategory(Rule):
    ...


class ReferenceToItemCategory(ScalarReferenceToRule[ItemCategory], target=ItemCategory):
    ...


class ArmorCategory(Rule):
    ...


class ReferenceToArmorCategory(
    ScalarReferenceToRule[ArmorCategory], target=ArmorCategory
):
    ...


class ArmorGroup(Rule, WithSources):
    ...


class ReferenceToArmorGroup(ScalarReferenceToRule[ArmorGroup], target=ArmorGroup):
    ...


class WeaponCategory(Rule):
    ...


class ReferenceToWeaponCategory(
    ScalarReferenceToRule[WeaponCategory], target=WeaponCategory
):
    ...


class WeaponGroup(Rule, WithSources):
    ...


class ReferenceToWeaponGroup(ScalarReferenceToRule[WeaponGroup], target=WeaponGroup):
    ...


class DamageCategory(Rule, WithSources):
    ...


class ReferenceToDamageCategory(
    ScalarReferenceToRule[DamageCategory], target=DamageCategory
):
    ...


class DamageType(Rule, WithSources):
    damagecategory: Annotated[
        ReferenceToDamageCategory,
        Field(description=("Specific rules apply for each damage category.")),
    ]


class ReferenceToDamageType(ScalarReferenceToRule[DamageType], target=DamageType):
    ...


class Frequency(Rule, WithSources):
    ...


class ReferenceToFrequency(ScalarReferenceToRule[Frequency], target=Frequency):
    ...


class SampleTasks(TypedDict, total=False):
    untrained: str
    trained: str
    expert: str
    master: str
    legendary: str


class Activity(Rule, EffectMixin, WithSources, WithTraits):
    actioncategory: Annotated[
        ReferenceToActionCategory,
        Field(..., description="Action category for this action."),
    ]
    actioncost: Annotated[
        ReferenceToActionCost,
        Field(
            ...,
            description="Opportunity cost for this action.",
        ),
    ]
    requirement: Annotated[
        str | None,
        Field(description="Requirements for this action."),
    ] = None
    trigger: Annotated[
        str | None,
        Field(
            ...,
            description="Trigger for this action.",
        ),
    ] = None
    frequency: Annotated[ReferenceToFrequency | None, Field(description="")] = None

    effects: Annotated[list[Effect], Field(description="", default_factory=list)]

    sample_tasks: Annotated[SampleTasks, Field(description="", default_factory=dict)]

    additional_description: Annotated[str | None, Field(description="")]

    @validator("trigger", pre=True)
    def _coerce_trigger(cls, v):  # type: ignore[no-untyped-def] # noqa: N805, ANN001, ANN202
        return v or None

    @root_validator(pre=True)
    def _coerce_default_actioncategory(cls, values):  # type: ignore[no-untyped-def] # noqa: N805, ANN001, ANN202
        values.setdefault("actioncategory", "Other")
        return values


class ReferenceToActivity(ScalarReferenceToRule[Activity], target=Activity):
    ...


class AnnotatedReferenceToActivity(ComplexReferenceToRule[Activity], target=Activity):
    note: Annotated[
        str,
        Field(
            description=(
                "Additional information about the use of this activity in "
                "its context."
            )
        ),
    ]


class Item(Rule, WithSources, WithTraits):
    bulk: Annotated[float, Field(..., description="Bulk value for this item.")]
    level: Annotated[
        int,
        Field(
            description=(
                "Represents the item's complexity and any magic used in "
                "its construction."
            )
        ),
    ] = 0
    price: Annotated[
        int | None,
        Field(
            description=(
                "The amount of currency it typically takes to purchase that "
                "item. "
                "The Price is expressed in copper pieces. "
                "A pice of 0 is typically free, while a null Price means "
                "the item cannot be purchased."
            )
        ),
    ] = None
    amount: Annotated[
        int,
        Field(
            description=(
                "The amount of individual units fo this item. "
                "This is more than one with ammunition, for instance."
            )
        ),
    ] = 1
    hands: Annotated[
        int,
        Field(description=("How many hands are occupied while using this item.")),
    ] = 0
    activities: Annotated[
        list[AnnotatedReferenceToActivity],
        Field(
            description="Activities provided by this item.",
            default_factory=list,
        ),
    ]
    itemcategory: Annotated[
        ReferenceToItemCategory,
        Field(description="A discriminator for different kind of items."),
    ]
    craft_requirements: Annotated[
        str | None,
        Field(description="Requirements for crafting this items, as prose."),
    ] = None

    @root_validator(pre=True)
    def _coerce_price(cls, values):  # type: ignore[no-untyped-def] # noqa: N805, ANN001, ANN202
        price_gp = values.get("price_gp", None)
        price_sp = values.get("price_sp", None)
        price_cp = values.get("price_cp", None)

        if all(_price is None for _price in [price_cp, price_sp, price_gp]):
            values.setdefault("price", None)
        else:
            price = 0
            price += (price_gp or 0) * 100
            price += (price_sp or 0) * 10
            price += price_cp or 0
            values.setdefault("price", price)

        return values

    @validator("bulk", pre=True)
    def _coerce_bulk(cls, v):  # type: ignore[no-untyped-def] # noqa: N805, ANN001, ANN202
        if v in ("", "-", None):
            return 0.0
        if v == "L":
            return 0.1

        return v

    @root_validator(pre=True)
    def _default_activities(cls, values):  # type: ignore[no-untyped-def] # noqa: N805, ANN001, ANN202
        values.setdefault("activities", [])
        return values


class Ammunition(Item):
    @root_validator(pre=True)
    def _coerce_itemcategory(cls, values):  # type: ignore[no-untyped-def] # noqa: N805, ANN001, ANN202
        values.setdefault("itemcategory", "Ammunition")

        return values


class Gear(Item):
    @root_validator(pre=True)
    def _coerce_itemcategory(cls, values):  # type: ignore[no-untyped-def] # noqa: N805, ANN001, ANN202
        values.setdefault("itemcategory", "UNCLASSIFIED")

        return values


class Armor(Item):
    ac_bonus: Annotated[
        int,
        Field(
            description=(
                "This number is the item bonus you add for the armor when "
                "determining Armor Class."
            )
        ),
    ]
    category: Annotated[
        ReferenceToArmorCategory,
        Field(
            description=(
                "The armor's category "
                "— unarmored, light armor, medium armor, or heavy armor —"
                "indicates which proficiency bonus you use while wearing "
                "the armor."
            )
        ),
    ]
    group: Annotated[
        ReferenceToArmorGroup | None,
        Field(
            description=(
                "Each type of medium and heavy armor belongs to an armor group, "
                "which classifies it with similar types of armor. "
                "Some abilities reference armor groups, typically to "
                "grant armor specialization effects."
            )
        ),
    ] = None
    check_penalty: Annotated[
        int,
        Field(
            description=(
                "While wearing your armor, you take this penalty to "
                "Strength- and Dexterity-based skill checks, "
                "except for those that have the attack trait. "
                "If you meet the armor's Strength threshold, "
                "you don't take this penalty."
            )
        ),
    ] = 0
    speed_penalty: Annotated[
        int,
        Field(
            description=(
                "While wearing a suit of armor, you take the penalty listed in "
                "this entry to your Speed, as well as to any other movement "
                "types you have, such as a climb Speed or swim Speed, to a "
                "minimum Speed of 5 feet. If you meet the armor's Strength "
                "threshold, you reduce the penalty by 5 feet."
            )
        ),
    ] = 0
    strength: Annotated[
        int,
        Field(
            description=(
                "This entry indicates the Strength score at which you are "
                "strong enough to overcome some of the armor's penalties. "
                "If your Strength is equal to or greater than this value, "
                "you no longer take the armor's check penalty, and "
                "you decrease the Speed penalty by 5 feet (to no penalty "
                "if the penalty was -5 feet, or to a -5 foot penalty if the "
                "penalty was -10 feet)."
            )
        ),
    ] = 0
    dex_cap: Annotated[
        int | None,
        Field(
            description=(
                "This number is the maximum amount of your Dexterity modifier "
                "that can apply to your AC while you are wearing a given suit "
                "of armor. "
                "For example, if you have a Dexterity modifier of +4 and "
                "you are wearing a suit of half plate, you apply only a "
                "+1 bonus from your Dexterity modifier to your AC while "
                "wearing that armor."
            )
        ),
    ] = None

    @root_validator(pre=True)
    def _coerce_itemcategory(cls, values):  # type: ignore[no-untyped-def] # noqa: N805, ANN001, ANN202
        values.setdefault("itemcategory", "Armor")

        return values

    @validator("check_penalty", "speed_penalty", "strength", pre=True)
    def _coerce_stats_to_zero(cls, v):  # type: ignore[no-untyped-def] # noqa: N805, ANN001, ANN202
        return v or 0


class Weapon(Rule, WithSources, WithTraits):
    category: Annotated[
        ReferenceToWeaponCategory,
        Field(
            description=(
                "Weapons fall into broad categories depending on how much "
                "damage they deal and what traits they have. "
                "Martial weapons generally deal more damage than simple "
                "weapons, and advanced weapons generally have more "
                "advantageous traits than martial weapons with the same damage. "
                "Generally, you'll want to select weapons that deal more "
                "damage, but if you're a highly skilled combatant, you might "
                "want to pick a weapon with interesting traits, even if it has "
                "a lower weapon damage die. You can also purchase multiple "
                "weapons within your budget, allowing you to switch between "
                "them for different situations."
            )
        ),
    ]
    group: Annotated[
        ReferenceToWeaponGroup,
        Field(
            description=(
                "A weapon or unarmed attack's group classifies it with similar "
                "weapons. Groups affect some abilities and what the weapon "
                "does on a critical hit if you have access to that weapon or "
                "unarmed attack's critical specialization effects"
            )
        ),
    ]
    damagetype: Annotated[
        ReferenceToDamageType,
        Field(description="The damage type for this weapon"),
    ]
    dice_size: Annotated[
        int,
        Field(
            description=(
                "Each weapon lists the damage die used for its damage roll. "
                "A standard weapon deals one die of damage, but a magical "
                "striking rune can increase the number of dice rolled, as can "
                "some special actions and spells. These additional dice use "
                "the same die size as the weapon or unarmed attack's normal "
                "damage die."
            )
        ),
    ]
    range: Annotated[  # noqa: A003
        int | None,
        Field(
            description=(
                "Ranged and thrown weapons have a range increment. "
                "Attacks with these weapons work normally up to that distance. "
                "Attack rolls beyond a weapon's range increment take a "
                "-2 penalty for each additional multiple of that increment "
                "between you and the target. "
                "Attacks beyond the sixth range increment are impossible."
            )
        ),
    ] = None
    reload: Annotated[
        int | None,
        Field(
            description=(
                "While all weapons need some amount of time to get into "
                "position, many ranged weapons also need to be loaded and "
                "reloaded. This entry indicates how many Interact actions "
                "it takes to reload such weapons. "
                "This can be 0 if drawing ammunition and firing the weapon "
                "are part of the same action. "
                "If an item takes 2 or more actions to reload, "
                "the GM determines whether they must be performed together "
                "as an activity, or you can spend some of those actions "
                "during one turn and the rest during your next turn. "
                "An item with an entry of “—” must be drawn to be thrown, "
                "which usually takes an Interact action just like drawing "
                "any other weapon. Reloading a ranged weapon and drawing a "
                "thrown weapon both require a free hand. Switching your grip "
                "to free a hand and then to place your hands in the grip "
                "necessary to wield the weapon are both included in the "
                "actions you spend to reload a weapon."
            )
        ),
    ] = None

    @root_validator(pre=True)
    def _coerce_itemcategory(cls, values):  # type: ignore[no-untyped-def] # noqa: N805, ANN001, ANN202
        values.setdefault("itemcategory", "Weapons")

        return values


class Size(Rule):
    reach_long: Annotated[int, Field(description="")]

    reach_tall: Annotated[int, Field(description="")]

    space: Annotated[int, Field(description="")]


class ReferenceToSize(ScalarReferenceToRule[Size], target=Size):
    ...


class LanguageRarity(Rule, WithSources):
    ...


class ReferenceToLanguageRarity(
    ScalarReferenceToRule[LanguageRarity], target=LanguageRarity
):
    ...


class Language(Rule, WithSources):
    rarity: Annotated[ReferenceToLanguageRarity, Field(description="")]

    speakers: Annotated[str, Field(description="")]


class ReferenceToLanguage(ScalarReferenceToRule[Language], target=Language):
    ...


class Sense(Rule, WithSources):
    ...


class ReferenceToSense(ScalarReferenceToRule[Sense], target=Sense):
    ...


class Skill(Rule, WithSources):
    key_ability: Annotated[ReferenceToAbilityScore, Field(description="")]
    untrained_activities: Annotated[
        list[Activity],
        Field(
            description="Activities provided by this skill when not trained.",
            default_factory=dict,
        ),
    ]
    trained_activities: Annotated[
        list[Activity],
        Field(
            description="Activities provided by this skill when trained.",
            default_factory=dict,
        ),
    ]

    untrained_activities_description: Annotated[
        str | None,
        Field(
            description="",
        ),
    ]

    trained_activities_description: Annotated[
        str | None,
        Field(
            description="",
        ),
    ]


class ReferenceToSkill(ScalarReferenceToRule[Skill], target=Skill):
    ...


class SkillCheck(ComplexReferenceToRule[Skill], target=Skill):
    dc: Annotated[int, Field(description="")]
    trait: Annotated[ReferenceToTrait | None, Field(description="")]


class Condition(Rule, WithSources):
    short_description: Annotated[str, Field(description="Short description.")]


class ReferenceToCondition(ScalarReferenceToRule[Condition], target=Condition):
    ...


class MovementType(Rule, WithSources):
    ...


class ReferenceToMovementType(ScalarReferenceToRule[MovementType], target=MovementType):
    ...


class BonusType(Rule, WithSources):
    ...


class ReferenceToBonusType(ScalarReferenceToRule[BonusType], target=BonusType):
    ...


class FeatPrerequisite(BaseModel):
    descr: str | None = None
    feat: str | None = None


class Feat(Rule, WithSources, WithTraits):
    level: Annotated[int, Field(description="Minimum level.")] = 0

    prerequisites: Annotated[
        list[FeatPrerequisite], Field(description="", default_factory=list)
    ]

    activities: Annotated[list[Activity], Field(description="", default_factory=list)]

    @root_validator(pre=True)
    def _coerce_activity_names(cls, values):  # type: ignore[no-untyped-def] # noqa: N805, ANN001, ANN202
        if not values:
            return values

        feat_name = values["name"]

        abilities = values.get("activities", [])

        if len(abilities) == 1:
            abilities[0].setdefault("name", feat_name)
            return values

        i = 0

        for ability in abilities:
            i += 1
            ability.setdefault("name", f"{feat_name} {i}")

        return values


class ReferenceToFeat(ScalarReferenceToRule[Feat], target=Feat):
    ...


class Ancestry(Rule, WithSources, WithTraits):
    boosts: Annotated[
        list[ReferenceToAbilityScore],
        Field(description="", default_factory=list),
    ]
    flaws: Annotated[
        list[ReferenceToAbilityScore],
        Field(description="", default_factory=list),
    ]
    hp: Annotated[int, Field(description="")]
    senses: Annotated[ReferenceToSense | None, Field(description="")]
    size: Annotated[ReferenceToSize, Field(description="")]
    speed: Annotated[int, Field(description="")]

    @validator("boosts", "flaws", pre=True)
    def _coerce_ability_modifiers(cls, v):  # type: ignore[no-untyped-def] # noqa: N805, ANN001, ANN202
        return v or []


class ReferenceToAncestry(ScalarReferenceToRule[Ancestry], target=Ancestry):
    ...


class Heritage(Rule, WithSources):
    feat: Annotated[ReferenceToFeat | None, Field(description="")]
    ancestry: Annotated[ReferenceToAncestry, Field(description="")]


class ReferenceToHeritage(ScalarReferenceToRule[Heritage], target=Heritage):
    ...


class Background(Rule, WithSources):
    boost_choices: Annotated[
        list[ReferenceToAbilityScore],
        Field(description="", default_factory=list),
    ]
    is_adventure_path_specific: Annotated[bool, Field(description="")]


class ReferenceToBackground(ScalarReferenceToRule[Background], target=Background):
    ...


class SpellSchool(Rule, WithSources):
    ...


class ReferenceToSpellSchool(ScalarReferenceToRule[SpellSchool], target=SpellSchool):
    ...


class SpellComponent(Rule, WithSources):
    ...


class ReferenceToSpellComponent(
    ScalarReferenceToRule[SpellComponent], target=SpellComponent
):
    ...


class SpellTradition(Rule):
    ...


class ReferenceToSpellTradition(
    ScalarReferenceToRule[SpellTradition], target=SpellTradition
):
    ...


class SpellType(Rule):
    ...


class ReferenceToSpellType(ScalarReferenceToRule[SpellType], target=SpellType):
    ...


class SavingThrow(Rule, WithSources):
    ...


class ReferenceToSavingThrow(ComplexReferenceToRule[SavingThrow], target=SavingThrow):
    basic: Annotated[
        bool,
        Field(
            description=(
                "Sometimes you will be called on to attempt a basic saving"
                "throw. This type of saving throw works just like any"
                "other saving throw—the “basic” part refers to the effects."
                "For a basic save, you'll attempt the check and determine"
                "whether you critically succeed, succeed, fail, or critically"
                "fail like you would any other saving throw. Then one of"
                "the following outcomes applies based on your degree of"
                "success—no matter what caused the saving throw."
                "Critical Success You take no damage from the spell, hazard, or"
                "effect that caused you to attempt the save."
                "Success You take half the listed damage from the effect."
                "Failure You take the full damage listed from the effect."
                "Critical Failure You take double the listed damage from the effect."
            )
        ),
    ] = False


class Heightened(BaseModel):
    level: str
    description: str


class _SpellActivity(Activity):
    components: Annotated[
        list[ReferenceToSpellComponent],
        Field(description="", default_factory=list),
    ]


class Spell(Rule, WithSources, WithTraits):
    cast: Annotated[str, Field(description="")]
    spelltype: Annotated[ReferenceToSpellType, Field(description="")]
    level: Annotated[
        int,
        Field(description=("")),
    ]

    traditions: Annotated[
        list[ReferenceToSpellTradition],
        Field(description="", default_factory=list),
    ]
    heightened: Annotated[
        list[Heightened,],
        Field(
            description=(
                "If the spell can be heightened, the effects of heightening it "
                "appear at the end of the stat block."
            ),
            default_factory=list,
        ),
    ]
    activities: Annotated[
        list[_SpellActivity], Field(description="", default_factory=list)
    ]

    @validator("traditions", pre=True)
    def _coerce_traditions(cls, v):  # type: ignore[no-untyped-def] # noqa: N805, ANN001, ANN202
        return v or []

    @root_validator(pre=True)
    def _coerce_activity_names(cls, values):  # type: ignore[no-untyped-def] # noqa: N805, ANN001, ANN202
        if not values:
            return values

        spell_name = values["name"]

        abilities = values.get("activities", [])

        if len(abilities) == 1:
            abilities[0].setdefault("name", spell_name)
            return values

        i = 0

        for ability in abilities:
            i += 1
            ability.setdefault("name", f"{spell_name} {i}")

        return values


class ReferenceToSpell(ScalarReferenceToRule[Spell], target=Spell):
    pass


class ClassProgressionItem(BaseModel):
    level: Annotated[
        int, Field(description="Class level where this progression applies.")
    ]
    items: Annotated[list[str], Field(description="", default_factory=list)]


class Class(Rule, WithSources, WithTraits):
    progression: Annotated[
        list[ClassProgressionItem],
        Field(
            description="Per level progression for this class",
            default_factory=list,
        ),
    ]


class ReferenceToClass(ScalarReferenceToRule[Class], target=Class):
    pass


class Staff(Item):
    item_bonus: Annotated[int, Field(description="FIXME")] = 0
    spells: Annotated[
        list[ReferenceToSpell],
        Field(description="Spells included in the staff", default_factory=list),
    ]

    @root_validator(pre=True)
    def _coerce_hands(cls, values):  # type: ignore[no-untyped-def] # noqa: N805, ANN001, ANN202
        values.setdefault("hands", 1)
        return values

    @root_validator(pre=True)
    def _default_item_bonus(cls, values):  # type: ignore[no-untyped-def] # noqa: N805, ANN001, ANN202
        if values.get("item_bonus", None) is None:
            values["item_bonus"] = 0

        return values


class AbilityModifiers(BaseModel):
    strength: Annotated[int, Field(description="", alias="str_mod")]
    dexterity: Annotated[int, Field(description="", alias="dex_mod")]
    constitution: Annotated[int, Field(description="", alias="con_mod")]
    intelligence: Annotated[int, Field(description="", alias="int_mod")]
    wisdom: Annotated[int, Field(description="", alias="wis_mod")]
    charisma: Annotated[int, Field(description="", alias="cha_mod")]


class BaseStatBlock(Rule, WithSources, WithTraits):
    level: Annotated[
        int,
        Field(description=("")),
    ] = 0
    size: Annotated[ReferenceToSize, Field(description="")]
    perception: Annotated[int, Field(description="")]
    ac: Annotated[int, Field(description="")]
    hit_points: Annotated[int, Field(description="", alias="hp")]
    alignment: Annotated[ReferenceToAlignment, Field(description="")]
    recall_knowledge: Annotated[
        list[SkillCheck], Field(description="", default_factory=list)
    ]

    ability_modifiers: Annotated[
        AbilityModifiers, Field(description="", alias="ability_mods")
    ]
    languages: Annotated[
        list[ReferenceToLanguage], Field(description="", default_factory=list)
    ]

    active_abilities: Annotated[
        list[Activity], Field(description="", default_factory=list)
    ]

    automatic_abilities: Annotated[
        list[Activity], Field(description="", default_factory=list)
    ]

    sense_abilities: Annotated[
        list[Activity], Field(description="", default_factory=list)
    ]

    hit_points_ability: Annotated[
        str | None, Field(description="", alias="hp_misc")
    ] = None


class Monster(BaseStatBlock):
    ...
