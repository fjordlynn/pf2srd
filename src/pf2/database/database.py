from __future__ import annotations

from collections import defaultdict
from enum import Enum
from logging import getLogger
from pathlib import Path
from typing import (
    TYPE_CHECKING,
    Any,
    Callable,
    Iterator,
    Literal,
    Self,
    Sequence,
    TypeVar,
    overload,
)

from pf2.database import Rule, RuleId, SourceFile
from pf2.database.pf2srd import (
    AbilityScore,
    ActionCategory,
    ActionCost,
    Activity,
    Alignment,
    Ammunition,
    Ancestry,
    Armor,
    ArmorCategory,
    ArmorGroup,
    Background,
    BonusType,
    Bulk,
    Class,
    Condition,
    DamageCategory,
    DamageType,
    Effect,
    Feat,
    Frequency,
    Gear,
    Heritage,
    ItemCategory,
    Language,
    LanguageRarity,
    MovementType,
    Publisher,
    SavingThrow,
    Sense,
    Size,
    Skill,
    Source,
    Spell,
    SpellComponent,
    SpellSchool,
    SpellTradition,
    SpellType,
    Staff,
    Trait,
    TraitType,
    Weapon,
    WeaponCategory,
    WeaponGroup,
)
from pf2.yaml import make_yaml_dump, make_yaml_load

if TYPE_CHECKING:
    from sphinx.environment import BuildEnvironment

    from pf2.yaml import JsonLike

    RuleType = TypeVar("RuleType", bound=Rule)

log = getLogger(__name__)


class KindEnum(str, Enum):
    PUBLISHER: str = "publisher"
    SOURCE: str = "source"
    TRAITTYPE: str = "traittype"
    TRAIT: str = "trait"
    ALIGNMENT: str = "alignment"
    ABILITYSCORE: str = "abilityscore"
    ACTIONCATEGORY: str = "actioncategory"
    ACTIONCOST: str = "actioncost"
    EFFECT: str = "effect"
    BULK: str = "bulk"
    ITEMCATEGORY: str = "itemcategory"
    ARMORCATEGORY: str = "armorcategory"
    ARMORGROUP: str = "armorgroup"
    WEAPONGROUP: str = "weapongroup"
    WEAPONCATEGORY: str = "weaponcategory"
    DAMAGECATEGORY: str = "damagecategory"
    DAMAGETYPE: str = "damagetype"
    FREQUENCY: str = "frequency"
    ACTIVITY: str = "activity"
    AMMUNITION: str = "ammunition"
    GEAR: str = "gear"
    ARMOR: str = "armor"
    WEAPON: str = "weapon"
    SIZE: str = "size"
    LANGUAGERARITY: str = "languagerarity"
    LANGUAGE: str = "language"
    SENSE: str = "sense"
    SKILL: str = "skill"
    CONDITION: str = "condition"
    MOVEMENTTYPE: str = "movementtype"
    BONUSTYPE: str = "bonustype"
    FEAT: str = "feat"
    ANCESTRY: str = "ancestry"
    HERITAGE: str = "heritage"
    BACKGROUND: str = "background"
    SPELLSCHOOL: str = "spellschool"
    SPELLCOMPONENT: str = "spellcomponent"
    SPELLTRADITION: str = "spelltradition"
    SPELLTYPE: str = "spelltype"
    SAVINGTHROW: str = "savingthrow"
    SPELL: str = "spell"
    CLASS: str = "class"
    STAFF: str = "staff"
    SOURCEFILE: str = "sourcefile"
    RULE: str = "rule"

    def get_model_cls(self) -> type[Rule]:  # noqa: PLR0911
        match self:
            case KindEnum.PUBLISHER:
                return Publisher
            case KindEnum.SOURCE:
                return Source
            case KindEnum.TRAITTYPE:
                return TraitType
            case KindEnum.TRAIT:
                return Trait
            case KindEnum.ALIGNMENT:
                return Alignment
            case KindEnum.ABILITYSCORE:
                return AbilityScore
            case KindEnum.ACTIONCATEGORY:
                return ActionCategory
            case KindEnum.ACTIONCOST:
                return ActionCost
            case KindEnum.EFFECT:
                return Effect
            case KindEnum.BULK:
                return Bulk
            case KindEnum.ITEMCATEGORY:
                return ItemCategory
            case KindEnum.ARMORCATEGORY:
                return ArmorCategory
            case KindEnum.ARMORGROUP:
                return ArmorGroup
            case KindEnum.WEAPONGROUP:
                return WeaponGroup
            case KindEnum.WEAPONCATEGORY:
                return WeaponCategory
            case KindEnum.DAMAGECATEGORY:
                return DamageCategory
            case KindEnum.DAMAGETYPE:
                return DamageType
            case KindEnum.FREQUENCY:
                return Frequency
            case KindEnum.ACTIVITY:
                return Activity
            case KindEnum.AMMUNITION:
                return Ammunition
            case KindEnum.GEAR:
                return Gear
            case KindEnum.ARMOR:
                return Armor
            case KindEnum.WEAPON:
                return Weapon
            case KindEnum.SIZE:
                return Size
            case KindEnum.LANGUAGERARITY:
                return LanguageRarity
            case KindEnum.LANGUAGE:
                return Language
            case KindEnum.SENSE:
                return Sense
            case KindEnum.SKILL:
                return Skill
            case KindEnum.CONDITION:
                return Condition
            case KindEnum.MOVEMENTTYPE:
                return MovementType
            case KindEnum.BONUSTYPE:
                return BonusType
            case KindEnum.FEAT:
                return Feat
            case KindEnum.ANCESTRY:
                return Ancestry
            case KindEnum.HERITAGE:
                return Heritage
            case KindEnum.BACKGROUND:
                return Background
            case KindEnum.SPELLSCHOOL:
                return SpellSchool
            case KindEnum.SPELLCOMPONENT:
                return SpellComponent
            case KindEnum.SPELLTRADITION:
                return SpellTradition
            case KindEnum.SPELLTYPE:
                return SpellType
            case KindEnum.SAVINGTHROW:
                return SavingThrow
            case KindEnum.SPELL:
                return Spell
            case KindEnum.CLASS:
                return Class
            case KindEnum.STAFF:
                return Staff
            case KindEnum.SOURCEFILE:
                return SourceFile
            case KindEnum.RULE:
                return Rule
            case _:
                raise KeyError(self)


class NotFoundError(KeyError):
    pass


class Database:
    def __init__(
        self,
        root_path: Path | str | Sequence[Path | str],
        yaml_load: Callable[[Path], JsonLike] | None,
        yaml_dumps: Callable[[JsonLike], str] | None,
    ):
        log.debug("Database initialisation")
        paths = [root_path] if isinstance(root_path, (Path, str)) else root_path
        self.root_paths = [Path(path) for path in paths]
        self.yaml_load = yaml_load or make_yaml_load().load  # type: ignore[attr-defined]
        self.yaml_dumps = yaml_dumps or make_yaml_dump().dumps  # type: ignore[attr-defined]

        self._files: dict[Path, Any] = {}
        self._cache: dict[RuleId, Rule] = {}
        self._cls_index: dict[type[Rule], list[RuleId]] = defaultdict(list)
        self._names_index: dict[RuleId, RuleId] = {}
        self._sourcename_index: dict[RuleId, tuple[Path, int, int]] = {}

        for filepath in self.root_paths:
            self.load_from_file(filepath)

    @classmethod
    def from_environment(cls: type[Self], env: BuildEnvironment) -> Database:
        if not hasattr(env.app, "_pf2srd_database"):
            database = Database([], make_yaml_load(), make_yaml_dump())
            env.app._pf2srd_database = database  # type: ignore[attr-defined] # noqa: SLF001
            data_directory = Path.cwd().joinpath("raw_data/data")
            for filepath in data_directory.rglob("*.yaml"):
                if (
                    filepath.name
                    in (
                        "feat.yaml",
                        "spell.yaml",
                    )
                    or "monster" in filepath.parts
                ):
                    continue
                database.load_from_file(filepath)

        return env.app._pf2srd_database  # type: ignore[no-any-return,attr-defined] # noqa: SLF001

    def load_from_file(self, filepath: Path) -> None:
        log.debug("loading: %s", filepath)
        raw_data = self.yaml_load(filepath)

        sourcefile = SourceFile.parse_obj(raw_data)
        self._register_one(sourcefile)

        # Some rules are hidden within others and are implicitely
        # registered under _instances. Ouch, FIXME
        for rules in Rule._instances.values():  # noqa: SLF001
            for rule in rules.values():
                self._register_one(rule)

        self.root_paths.append(filepath)

    def _register_one(self, rule: Rule) -> None:
        rule_primary_id = RuleId(str(rule.kind), str(rule.name))

        if rule_primary_id in self._cache:
            return

        # Main index for rules, primary name
        self._cache[rule_primary_id] = rule

        # Index under all parent classes
        for cls in [cls for cls in rule.__class__.__mro__ if issubclass(cls, Rule)]:
            self._cls_index[cls].append(rule_primary_id)

        # Index primary and additional names
        self._names_index[rule_primary_id] = rule_primary_id
        for alternate_name in rule.names:
            rule_atlernate_id = RuleId(str(rule_primary_id.kind), str(alternate_name))
            self._names_index[rule_atlernate_id] = rule_primary_id

    def find_all(self, kind: type[RuleType]) -> Iterator[RuleType]:
        for rule_id in self._cls_index[kind]:
            yield self._cache[rule_id]  # type: ignore[misc] # We manage type consistency

    def find_one(self, kind: type[RuleType], name: str) -> RuleType:
        try:
            rule_id = self._names_index[RuleId(str(kind.get_kind()), str(name))]
            return self._cache[rule_id]  # type: ignore[return-value] # We manage type consistency
        except KeyError as e:
            raise NotFoundError from e

    def get_sourcename(self, kind: type[RuleType], name: str) -> tuple[Path, int, int]:
        return self._sourcename_index[
            self._names_index[RuleId(str(kind.get_kind()), str(name))]
        ]

    def check(self) -> Report:
        return Report.parse_obj(
            {
                "name": "report",
                "kind": "report",
                "files": self.root_paths,
                "items": {rule.name: rule for rule in self._cache.values()},
            }
        )

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.PUBLISHER]) -> type[Publisher]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.SOURCE]) -> type[Source]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.TRAITTYPE]) -> type[TraitType]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.TRAIT]) -> type[Trait]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.ALIGNMENT]) -> type[Alignment]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.ABILITYSCORE]) -> type[AbilityScore]:
        ...

    @overload
    def get_model_cls(
        self, kind: Literal[KindEnum.ACTIONCATEGORY]
    ) -> type[ActionCategory]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.ACTIONCOST]) -> type[ActionCost]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.EFFECT]) -> type[Effect]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.BULK]) -> type[Bulk]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.ITEMCATEGORY]) -> type[ItemCategory]:
        ...

    @overload
    def get_model_cls(
        self, kind: Literal[KindEnum.ARMORCATEGORY]
    ) -> type[ArmorCategory]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.ARMORGROUP]) -> type[ArmorGroup]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.WEAPONGROUP]) -> type[WeaponGroup]:
        ...

    @overload
    def get_model_cls(
        self, kind: Literal[KindEnum.WEAPONCATEGORY]
    ) -> type[WeaponCategory]:
        ...

    @overload
    def get_model_cls(
        self, kind: Literal[KindEnum.DAMAGECATEGORY]
    ) -> type[DamageCategory]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.DAMAGETYPE]) -> type[DamageType]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.FREQUENCY]) -> type[Frequency]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.ACTIVITY]) -> type[Activity]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.AMMUNITION]) -> type[Ammunition]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.GEAR]) -> type[Gear]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.ARMOR]) -> type[Armor]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.WEAPON]) -> type[Weapon]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.SIZE]) -> type[Size]:
        ...

    @overload
    def get_model_cls(
        self, kind: Literal[KindEnum.LANGUAGERARITY]
    ) -> type[LanguageRarity]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.LANGUAGE]) -> type[Language]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.SENSE]) -> type[Sense]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.SKILL]) -> type[Skill]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.CONDITION]) -> type[Condition]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.MOVEMENTTYPE]) -> type[MovementType]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.BONUSTYPE]) -> type[BonusType]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.FEAT]) -> type[Feat]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.ANCESTRY]) -> type[Ancestry]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.HERITAGE]) -> type[Heritage]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.BACKGROUND]) -> type[Background]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.SPELLSCHOOL]) -> type[SpellSchool]:
        ...

    @overload
    def get_model_cls(
        self, kind: Literal[KindEnum.SPELLCOMPONENT]
    ) -> type[SpellComponent]:
        ...

    @overload
    def get_model_cls(
        self, kind: Literal[KindEnum.SPELLTRADITION]
    ) -> type[SpellTradition]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.SPELLTYPE]) -> type[SpellType]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.SAVINGTHROW]) -> type[SavingThrow]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.SPELL]) -> type[Spell]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.CLASS]) -> type[Class]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.STAFF]) -> type[Staff]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.SOURCEFILE]) -> type[SourceFile]:
        ...

    @overload
    def get_model_cls(self, kind: Literal[KindEnum.RULE]) -> type[Rule]:
        ...

    def get_model_cls(self, kind: KindEnum) -> type[Rule]:  # noqa: PLR0911
        match kind:
            case KindEnum.PUBLISHER:
                return Publisher
            case KindEnum.SOURCE:
                return Source
            case KindEnum.TRAITTYPE:
                return TraitType
            case KindEnum.TRAIT:
                return Trait
            case KindEnum.ALIGNMENT:
                return Alignment
            case KindEnum.ABILITYSCORE:
                return AbilityScore
            case KindEnum.ACTIONCATEGORY:
                return ActionCategory
            case KindEnum.ACTIONCOST:
                return ActionCost
            case KindEnum.EFFECT:
                return Effect
            case KindEnum.BULK:
                return Bulk
            case KindEnum.ITEMCATEGORY:
                return ItemCategory
            case KindEnum.ARMORCATEGORY:
                return ArmorCategory
            case KindEnum.ARMORGROUP:
                return ArmorGroup
            case KindEnum.WEAPONGROUP:
                return WeaponGroup
            case KindEnum.WEAPONCATEGORY:
                return WeaponCategory
            case KindEnum.DAMAGECATEGORY:
                return DamageCategory
            case KindEnum.DAMAGETYPE:
                return DamageType
            case KindEnum.FREQUENCY:
                return Frequency
            case KindEnum.ACTIVITY:
                return Activity
            case KindEnum.AMMUNITION:
                return Ammunition
            case KindEnum.GEAR:
                return Gear
            case KindEnum.ARMOR:
                return Armor
            case KindEnum.WEAPON:
                return Weapon
            case KindEnum.SIZE:
                return Size
            case KindEnum.LANGUAGERARITY:
                return LanguageRarity
            case KindEnum.LANGUAGE:
                return Language
            case KindEnum.SENSE:
                return Sense
            case KindEnum.SKILL:
                return Skill
            case KindEnum.CONDITION:
                return Condition
            case KindEnum.MOVEMENTTYPE:
                return MovementType
            case KindEnum.BONUSTYPE:
                return BonusType
            case KindEnum.FEAT:
                return Feat
            case KindEnum.ANCESTRY:
                return Ancestry
            case KindEnum.HERITAGE:
                return Heritage
            case KindEnum.BACKGROUND:
                return Background
            case KindEnum.SPELLSCHOOL:
                return SpellSchool
            case KindEnum.SPELLCOMPONENT:
                return SpellComponent
            case KindEnum.SPELLTRADITION:
                return SpellTradition
            case KindEnum.SPELLTYPE:
                return SpellType
            case KindEnum.SAVINGTHROW:
                return SavingThrow
            case KindEnum.SPELL:
                return Spell
            case KindEnum.CLASS:
                return Class
            case KindEnum.STAFF:
                return Staff
            case KindEnum.SOURCEFILE:
                return SourceFile
            case KindEnum.RULE:
                return Rule
            case _:
                raise KeyError(kind)


class Report(Rule):
    files: tuple[Path, ...]
