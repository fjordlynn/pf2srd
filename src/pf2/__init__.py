from __future__ import annotations


class MissingTargetClassError(ValueError):
    def __init__(self, cls_name: str):
        super().__init__(f"Reference needs a target class in {cls_name}")
