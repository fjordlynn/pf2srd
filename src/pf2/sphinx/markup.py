"""
Provides directive and documenters for the SRD autodocumentation.

Documenters know how to document things. They specific to a rule entry model
class (and thus are Generics).
"""
from __future__ import annotations

from functools import lru_cache
from typing import Any, Generic, Self, TypeVar

from sphinx.util.logging import getLogger

from pf2 import MissingTargetClassError
from pf2.database import Rule, SourceFile
from pf2.database.database import Database, KindEnum
from pf2.database.pf2srd import (
    AbilityScore,
    ActionCategory,
    ActionCost,
    Activity,
    Alignment,
    Ammunition,
    Ancestry,
    Armor,
    ArmorCategory,
    ArmorGroup,
    Background,
    BonusType,
    Bulk,
    Class,
    Condition,
    DamageCategory,
    DamageType,
    Effect,
    Feat,
    Frequency,
    Gear,
    Heritage,
    ItemCategory,
    Language,
    LanguageRarity,
    MovementType,
    Publisher,
    SavingThrow,
    Sense,
    Size,
    Skill,
    Source,
    Spell,
    SpellComponent,
    SpellSchool,
    SpellTradition,
    SpellType,
    Staff,
    Trait,
    TraitType,
    Weapon,
    WeaponCategory,
    WeaponGroup,
)
from pf2.sphinx.compendia import (
    ConstituentDocumenter,
    DocumenterBridge,
    ObjectIdentifier,
)
from pf2.sphinx.rstgen import (
    generate_datafields,
    generate_directive_footer,
    generate_rubric,
)

RuleType = TypeVar("RuleType", bound=Rule)

log = getLogger(__name__)


class Pf2SrdDocumenterAdapter(DocumenterBridge):
    def __init__(self, *args, **kwargs) -> None:  # type: ignore[no-untyped-def] # noqa: ANN002, ANN003
        super().__init__(*args, **kwargs)

        self.database = Database.from_environment(self.env)

    def get_documenter_class(
        self, object_id: ObjectIdentifier
    ) -> type[ConstituentDocumenter]:
        objtypes_to_documenter: dict[str, type[ConstituentDocumenter]] = {
            "publisher": PublisherDocumenter,
            "source": SourceDocumenter,
            "traittype": TraitTypeDocumenter,
            "trait": TraitDocumenter,
            "alignment": AlignmentDocumenter,
            "abilityscore": AbilityScoreDocumenter,
            "actioncategory": ActionCategoryDocumenter,
            "actioncost": ActionCostDocumenter,
            "effect": EffectDocumenter,
            "bulk": BulkDocumenter,
            "itemcategory": ItemCategoryDocumenter,
            "armorcategory": ArmorCategoryDocumenter,
            "armorgroup": ArmorGroupDocumenter,
            "weapongroup": WeaponGroupDocumenter,
            "weaponcategory": WeaponCategoryDocumenter,
            "damagecategory": DamageCategoryDocumenter,
            "damagetype": DamageTypeDocumenter,
            "frequency": FrequencyDocumenter,
            "activity": ActivityDocumenter,
            "ammunition": AmmunitionDocumenter,
            "gear": GearDocumenter,
            "armor": ArmorDocumenter,
            "weapon": WeaponDocumenter,
            "size": SizeDocumenter,
            "languagerarity": LanguageRarityDocumenter,
            "language": LanguageDocumenter,
            "sense": SenseDocumenter,
            "skill": SkillDocumenter,
            "condition": ConditionDocumenter,
            "movementtype": MovementTypeDocumenter,
            "bonustype": BonusTypeDocumenter,
            "feat": FeatDocumenter,
            "ancestry": AncestryDocumenter,
            "heritage": HeritageDocumenter,
            "background": BackgroundDocumenter,
            "spellschool": SpellSchoolDocumenter,
            "spellcomponent": SpellComponentDocumenter,
            "spelltradition": SpellTraditionDocumenter,
            "spelltype": SpellTypeDocumenter,
            "savingthrow": SavingThrowDocumenter,
            "spell": SpellDocumenter,
            "class": ClassDocumenter,
            "staff": StaffDocumenter,
            "sourcefile": SourceFileDocumenter,
            "rule": RuleDocumenter,
        }

        return objtypes_to_documenter[object_id.objtype]

    @lru_cache  # noqa: B019
    def get_sourcename(self, object_id: ObjectIdentifier) -> tuple[str, int]:
        cls = self.database.get_model_cls(KindEnum(object_id.objtype))  # type: ignore[call-overload] # https://gitlab.com/fjordlynn/pf2srd/-/issues/6
        obj = self.database.find_one(cls, object_id.name)
        return str(obj.sourcefile), int(obj.line_start if obj.line_start else 1)


class Pf2srdDocumenter(Generic[RuleType], ConstituentDocumenter):
    """
    Base documenter for rule entries.

    Documenters are Generics **and** need a reference to the model class
    they know how to document. This makes sure they can use the
    :class:`pf2.database.database.Database` properly to fetch the entries.
    """

    target_rule_cls: type[RuleType]

    def __init_subclass__(
        cls,
        /,
        target: type[RuleType] | None = None,
        **kwargs: Any,  # noqa: ANN401 Other kwargs are simply passed through
    ) -> None:
        """
        Make sure the target model class is passed in the class definition.

        This method is automatically invoked when a new subclass is imported
        and will set the :attr:`~target_rule_cls` attribute.

        Args:
            target:
                The target model class.

                This can be None when the documenter class name starts with
                ``_`` or ``Base``.
            kwargs:
                Any other class definition keyword argument.

        Raises:
            ValueError:
                When a subclass should have a target model class is
                defined without one.
        """
        super().__init_subclass__(**kwargs)

        concrete_target = not cls.__name__.startswith(
            "_"
        ) and not cls.__name__.startswith("Base")

        if target and issubclass(target, Rule):
            cls.target_rule_cls = target
            return

        if concrete_target:
            raise MissingTargetClassError(cls.__name__)

        cls.target_rule_cls = Rule

    def get_signatures(self) -> tuple[str, ...]:
        return tuple(self.get_object().names)

    @property
    def database(self) -> Database:
        return Database.from_environment(self.env)

    def get_object(self) -> RuleType:
        return self.database.find_one(
            self.target_rule_cls, self.documented_object_identifier.name
        )

    def make_object_identifier(self, obj: Rule) -> ObjectIdentifier:
        return ObjectIdentifier(self.domain_name, obj.kind, obj.name)

    def generate_content(self) -> None:
        """
        Generate the documentation content for a rule entry.

        First we generate a few key information as a two-column table
        with one stub columns, which displays more cleanly than a
        field list in our HTML theme.

        Then we generate the description.

        Finaly we generate the table of children.

        Notes:
            This method should probably not be overwritten in subclasses.
        """
        self.generate_data_fields()
        self.generate_description()

    def generate_description(self) -> None:
        """
        Generate the description documentation of a rule entry.

        Notes:
            This is likely one method to overwrite in subclasses in order
            to customize the documented result for a given model class.
        """
        rule = self.get_object()

        self.bridge.yield_content(
            rule.description.strip() + "\n\n", self.documented_object_identifier
        )

        self.bridge.yield_content(
            generate_directive_footer(
                "description",
                content_indent=self.bridge.one_indent,
            ),
            self.documented_object_identifier,
        )

    def generate_data_fields(self) -> None:
        """
        Generate the base information fields of a rule entry.

        Notes:
            This method should probably not be overwritten in subclasses.
            Overwrite :meth:`~get_data_fields` instead.
        """
        data_fields = self.get_data_fields()

        self.bridge.yield_content(
            generate_datafields(
                tuple(data_fields.keys()),
                tuple(data_fields.values()),
                content_indent=self.bridge.one_indent,
            ),
            self.documented_object_identifier,
        )

    def get_data_fields(self) -> dict[str, str]:
        return {}

    @classmethod
    def get_table_headers(cls: type[Self]) -> tuple[str, ...]:
        return "Name", "Description"

    def get_table_row(self) -> tuple[str, ...]:
        obj = self.get_object()
        return obj.rst_role(domain=self.domain_name), obj.description


class PublisherDocumenter(Pf2srdDocumenter[Publisher], target=Publisher):
    objtype = directivetype = "publisher"


class SourceDocumenter(Pf2srdDocumenter[Source], target=Source):
    objtype = directivetype = "source"

    def get_signatures(self) -> tuple[str, ...]:
        source = self.get_object()

        return (source.name, source.short_name, source.abbreviation, source.pzocode)

    def generate_content(self) -> None:
        super().generate_content()

        rule = self.get_object()

        self.bridge.yield_content(
            generate_rubric(rule.copyright_block, "Copyright block"),
            self.documented_object_identifier,
        )

    def get_data_fields(self) -> dict[str, str]:
        entity = self.get_object()

        return {
            "Title": entity.title,
            "Publisher": entity.publisher.rst_role(),
            "Release": str(entity.release_date),
            "PZO Code": entity.pzocode,
            "ISBN": entity.isbn if entity.isbn else "",
        }


class TraitTypeDocumenter(Pf2srdDocumenter[TraitType], target=TraitType):
    objtype = directivetype = "traittype"


class TraitDocumenter(Pf2srdDocumenter[Trait], target=Trait):
    objtype = directivetype = "trait"

    @classmethod
    def get_table_headers(cls: type[Self]) -> tuple[str, ...]:
        return "Name", "Type", "Description"

    def get_table_row(self) -> tuple[str, ...]:
        obj = self.get_object()
        return (obj.rst_role(domain=self.domain_name), obj.traittype, obj.description)


class AlignmentDocumenter(Pf2srdDocumenter[Alignment], target=Alignment):
    objtype = directivetype = "alignment"


class AbilityScoreDocumenter(Pf2srdDocumenter[AbilityScore], target=AbilityScore):
    objtype = directivetype = "abilityscore"


class ActionCategoryDocumenter(Pf2srdDocumenter[ActionCategory], target=ActionCategory):
    objtype = directivetype = "actioncategory"


class ActionCostDocumenter(Pf2srdDocumenter[ActionCost], target=ActionCost):
    objtype = directivetype = "actioncost"


class EffectDocumenter(Pf2srdDocumenter[Effect], target=Effect):
    objtype = directivetype = "effect"

    def get_data_fields(self) -> dict[str, str]:
        effect = self.get_object()
        fields = {}

        if effect.range is not None:
            fields["Range"] = effect.range
        if effect.area is not None:
            fields["Area"] = effect.area
        if effect.target is not None:
            fields["Target"] = effect.target
        if effect.duration is not None:
            fields["Duration"] = effect.duration
        if effect.critical_success is not None:
            fields["Critical success"] = effect.critical_success
        if effect.success is not None:
            fields["Success"] = effect.success
        if effect.failure is not None:
            fields["Failure"] = effect.failure
        if effect.critical_failure is not None:
            fields["Critical failure"] = effect.critical_failure

        return fields


class BulkDocumenter(Pf2srdDocumenter[Bulk], target=Bulk):
    objtype = directivetype = "bulk"


class ItemCategoryDocumenter(Pf2srdDocumenter[ItemCategory], target=ItemCategory):
    objtype = directivetype = "itemcategory"


class ArmorCategoryDocumenter(Pf2srdDocumenter[ArmorCategory], target=ArmorCategory):
    objtype = directivetype = "armorcategory"


class ArmorGroupDocumenter(Pf2srdDocumenter[ArmorGroup], target=ArmorGroup):
    objtype = directivetype = "armorgroup"


class WeaponGroupDocumenter(Pf2srdDocumenter[WeaponGroup], target=WeaponGroup):
    objtype = directivetype = "weapongroup"


class WeaponCategoryDocumenter(Pf2srdDocumenter[WeaponCategory], target=WeaponCategory):
    objtype = directivetype = "weaponcategory"


class DamageCategoryDocumenter(Pf2srdDocumenter[DamageCategory], target=DamageCategory):
    objtype = directivetype = "damagecategory"


class DamageTypeDocumenter(Pf2srdDocumenter[DamageType], target=DamageType):
    objtype = directivetype = "damagetype"


class FrequencyDocumenter(Pf2srdDocumenter[Frequency], target=Frequency):
    objtype = directivetype = "frequency"


class ActivityDocumenter(Pf2srdDocumenter[Activity], target=Activity):
    objtype = directivetype = "activity"

    def generate_content(self) -> None:
        super().generate_content()

        activity = self.get_object()

        success = {}
        if activity.critical_success:
            success["Critical success"] = activity.critical_success
        if activity.success:
            success["Success"] = activity.success
        if activity.failure:
            success["Failure"] = activity.failure
        if activity.critical_failure:
            success["Critical failure"] = activity.critical_failure

        self.bridge.yield_content(
            generate_datafields(
                tuple(success.keys()),
                tuple(success.values()),
                title="Success level",
                content_indent=self.bridge.one_indent,
            ),
            self.documented_object_identifier,
        )

        for effect in activity.effects:
            self.bridge.generate(self.make_object_identifier(effect))

        self.bridge.yield_content(
            generate_datafields(
                [key.title() for key in activity.sample_tasks],
                tuple(str(value) for value in activity.sample_tasks.values()),
                f"Sample {activity.name} tasks",
                content_indent=self.bridge.one_indent,
            ),
            self.documented_object_identifier,
        )

        if activity.additional_description:
            self.bridge.yield_content(
                activity.additional_description.strip() + "\n\n",
                self.documented_object_identifier,
            )

    def get_data_fields(self) -> dict[str, str]:
        activity = self.get_object()
        fields = {}

        if activity.requirement:
            fields["Requirement"] = activity.requirement

        if activity.trigger:
            fields["Trigger"] = activity.trigger

        if activity.frequency:
            fields["Frequency"] = activity.frequency

        return fields


class AmmunitionDocumenter(Pf2srdDocumenter[Ammunition], target=Ammunition):
    objtype = directivetype = "ammunition"


class GearDocumenter(Pf2srdDocumenter[Gear], target=Gear):
    objtype = directivetype = "gear"


class ArmorDocumenter(Pf2srdDocumenter[Armor], target=Armor):
    objtype = directivetype = "armor"


class WeaponDocumenter(Pf2srdDocumenter[Weapon], target=Weapon):
    objtype = directivetype = "weapon"


class SizeDocumenter(Pf2srdDocumenter[Size], target=Size):
    objtype = directivetype = "size"


class LanguageRarityDocumenter(Pf2srdDocumenter[LanguageRarity], target=LanguageRarity):
    objtype = directivetype = "languagerarity"


class LanguageDocumenter(Pf2srdDocumenter[Language], target=Language):
    objtype = directivetype = "language"


class SenseDocumenter(Pf2srdDocumenter[Sense], target=Sense):
    objtype = directivetype = "sense"


class SkillDocumenter(Pf2srdDocumenter[Skill], target=Skill):
    objtype = directivetype = "skill"

    def generate_content(self) -> None:
        # TODO(cblegare): refactor and simplify  # noqa: FIX002
        # https://gitlab.com/fjordlynn/pf2srd/-/issues/4
        super().generate_content()
        skill = self.get_object()

        if skill.trained_activities:
            self.bridge.yield_content(
                generate_rubric("", f"{skill.name} trained actions", make_footer=False),
                self.documented_object_identifier,
            )

            if skill.trained_activities_description:
                self.bridge.yield_content(
                    skill.trained_activities_description.strip() + "\n\n",
                    self.documented_object_identifier,
                )

            for activity in skill.trained_activities:
                self.bridge.generate(self.make_object_identifier(activity))

        if skill.untrained_activities:
            self.bridge.yield_content(
                generate_rubric(
                    "", f"{skill.name} untrained actions", make_footer=False
                ),
                self.documented_object_identifier,
            )

            if skill.untrained_activities_description:
                self.bridge.yield_content(
                    skill.untrained_activities_description.strip() + "\n\n",
                    self.documented_object_identifier,
                )

            for activity in skill.untrained_activities:
                self.bridge.generate(self.make_object_identifier(activity))

    def get_data_fields(self) -> dict[str, str]:
        skill = self.get_object()

        return {
            "Key Ability": skill.key_ability.rst_role(),
            "Trained Actions": ", ".join(
                [activity.rst_role() for activity in skill.trained_activities]
            ),
            "Untrained Actions": ", ".join(
                [activity.rst_role() for activity in skill.untrained_activities]
            ),
        }


class ConditionDocumenter(Pf2srdDocumenter[Condition], target=Condition):
    objtype = directivetype = "condition"


class MovementTypeDocumenter(Pf2srdDocumenter[MovementType], target=MovementType):
    objtype = directivetype = "movementtype"


class BonusTypeDocumenter(Pf2srdDocumenter[BonusType], target=BonusType):
    objtype = directivetype = "bonustype"


class FeatDocumenter(Pf2srdDocumenter[Feat], target=Feat):
    objtype = directivetype = "feat"


class AncestryDocumenter(Pf2srdDocumenter[Ancestry], target=Ancestry):
    objtype = directivetype = "ancestry"


class HeritageDocumenter(Pf2srdDocumenter[Heritage], target=Heritage):
    objtype = directivetype = "heritage"


class BackgroundDocumenter(Pf2srdDocumenter[Background], target=Background):
    objtype = directivetype = "background"


class SpellSchoolDocumenter(Pf2srdDocumenter[SpellSchool], target=SpellSchool):
    objtype = directivetype = "spellschool"


class SpellComponentDocumenter(Pf2srdDocumenter[SpellComponent], target=SpellComponent):
    objtype = directivetype = "spellcomponent"


class SpellTraditionDocumenter(Pf2srdDocumenter[SpellTradition], target=SpellTradition):
    objtype = directivetype = "spelltradition"


class SpellTypeDocumenter(Pf2srdDocumenter[SpellType], target=SpellType):
    objtype = directivetype = "spelltype"


class SavingThrowDocumenter(Pf2srdDocumenter[SavingThrow], target=SavingThrow):
    objtype = directivetype = "savingthrow"


class SpellDocumenter(Pf2srdDocumenter[Spell], target=Spell):
    objtype = directivetype = "spell"


class ClassDocumenter(Pf2srdDocumenter[Class], target=Class):
    objtype = directivetype = "class"


class StaffDocumenter(Pf2srdDocumenter[Staff], target=Staff):
    objtype = directivetype = "staff"


class SourceFileDocumenter(Pf2srdDocumenter[SourceFile], target=SourceFile):
    objtype = directivetype = "sourcefile"


class RuleDocumenter(Pf2srdDocumenter[Rule], target=Rule):
    objtype = directivetype = "rule"
