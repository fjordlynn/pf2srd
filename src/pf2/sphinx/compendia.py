"""
Documenting constituent from external sources.

This module provides directives for automatically including documentation
for constituent from programmatically gathered information.
It works much like the Autodoc Sphinx extension, but since it aims a more
general purpose, only abstract class are provided here. You will need
to implement a bridge between your external sources and the documenters
provided here. Also, there are some differences with this implementation
from Autodoc which are highlighted in-place.

Your implementation will have access to the Sphinx environment, including
any configuration keys you may have provided, along with the top level
autodocumentation directive and all its arguments.
This needs to be sufficient to initialize your implementation.
"""
from __future__ import annotations

from contextlib import contextmanager
from typing import TYPE_CHECKING, Any, Callable, Generator, NamedTuple, TypeVar

from docutils import nodes
from docutils.parsers.rst import directives
from docutils.statemachine import StringList
from docutils.utils import Reporter, assemble_option_dict
from sphinx.ext.autodoc import Options
from sphinx.util import nested_parse_with_titles  # type: ignore[attr-defined]
from sphinx.util.docutils import SphinxDirective, switch_source_input
from sphinx.util.logging import getLogger
from sphinx_compendia.domain import Domain

if TYPE_CHECKING:
    from typing import ClassVar, Iterable, Self

    from docutils.nodes import Element, Node
    from docutils.parsers.rst.states import RSTState
    from docutils.statemachine import State
    from sphinx.environment import BuildEnvironment
    from sphinx.util.typing import OptionSpec


DocumentedObjectType = TypeVar("DocumentedObjectType")


log = getLogger(__name__)


class UnexpectedDocumenterClassError(TypeError):
    def __init__(self, object_id: ObjectIdentifier, documenter_class: type):
        super().__init__(
            f"Documenter of '{object_id}' was of unexpected "
            f"type '{documenter_class}'."
        )


class DummyOptionSpec(dict[str, Callable[[str], Any]]):
    """An option_spec allows any options."""

    def __bool__(self) -> bool:
        """Behaves like some options are defined."""
        return True

    def __getitem__(self, key: str) -> Callable[[str], str]:
        return lambda x: x


class ObjectIdentifier(NamedTuple):
    domain_name: str
    objtype: str
    name: str


class DocumenterBridge:
    """
    Adapt programmatic generation of content to documenters.

    Notes:
        To implement this class, you may need to inject dependencies
        in a service class of some sort. You can get initialization
        data from a few places.

        Configuration keys (set in ``conf.py``) are accessible as
        attributes under ``self.env.config``.

        While not recommended, you can also read environment variables
        and hardcode data file paths.
    """

    def __init__(  # noqa: PLR0913
        self,
        env: BuildEnvironment,
        state: State,
        reporter: Reporter,
        genopts: Options,
        one_indent: str = " " * 4,
        initial_indent_number: int = 0,
    ) -> None:
        r"""
        Create a documenter adapter with versatile but simple parameters.

        Args:
            env:
                This makes the configuration from conf.py available.
            state:
                This provides the parsing context.
            reporter:
                The reporter object manage internal system messages.
            genopts:
                The options from the calling directive (auto\* directive), used
                to seed the generated directive options.
            one_indent:
                The width of one indent.
            initial_indent_number:
                The number of indentation to start with.
        """
        self.env = env
        self.state = state
        self.reporter = reporter
        self.genopt = genopts
        self.record_dependencies: set[str] = set()
        self.result = StringList()
        self.one_indent = one_indent
        self.indent_number = initial_indent_number

    @contextmanager
    def indent(self) -> Generator[Self, None, None]:
        self.indent_number += 1
        yield self
        self.indent_number -= 1

    def get_documenter_class(
        self, object_id: ObjectIdentifier
    ) -> type[ConstituentDocumenter]:
        """
        Provide the documenter class for a given object identifier.

        Args:
            object_id:
                Identifier for the object to document.

        Notes:
            Subclasses must implement this method. In case of the Python
            auto documenter extension, there is a registry of documenter. We
            cannot use it because it is not partitioned by domain and because
            the Compendia documenter constructor is not compatible with the
            Python ones.
        """
        raise NotImplementedError

    def get_sourcename(self, object_id: ObjectIdentifier) -> tuple[str, int]:
        """
        Provide the source file and line number for a given object identifier.

        This is used for partial build and caching.

        Args:
            object_id:
                Identifier for the object to document.

        Notes:
             Subclasses must implement this method.
        """
        raise NotImplementedError

    def get_documenter(
        self,
        object_id: ObjectIdentifier,
    ) -> ConstituentDocumenter:
        """
        Create the documenter for a given object identifier.

        Args:
            object_id:
                Identifier for the object to document.
            indent:
                Base RestructuredText indentation.

        Returns:
            A documenter instance fit for the object identifier.
        """
        documenter_class = self.get_documenter_class(object_id)

        documenter_options = Options(
            assemble_option_dict(self.genopt.items(), documenter_class.option_spec)
        )

        return documenter_class(self, object_id, documenter_options)

    def generate(
        self,
        object_id: ObjectIdentifier,
        more_content: StringList | None = None,
    ) -> None:
        """
        Generate de documentation for a given object identifier.

        Args:
            object_id:
                Identifier for the object to document.
            more_content:
                Additional content to add to the object's documentation.
            indent:
                Initial indentation under which the RestructuredText
                should start.
        """
        # Get the first documenter
        documenter = self.get_documenter(object_id)

        # Record the object's sourcename as a dependency
        self.record_dependency(self.get_sourcename(object_id)[0])

        # Generate de documentation
        documenter.generate(more_content=more_content)

    def record_dependency(self, dependency: str) -> None:
        self.state.document.settings.record_dependencies.add(dependency)  # type: ignore[attr-defined]

    def yield_content(
        self,
        content: str | Iterable[str],
        object_id: ObjectIdentifier | None = None,
        sourcename: str | None = None,
        lineno: int = 0,
    ) -> None:
        if sourcename is None and object_id:
            sourcename, lineno = self.get_sourcename(object_id)

        indent = self.indent_number * self.one_indent

        if not content:
            self.result.append(indent, sourcename, lineno)
            return

        if isinstance(content, str):
            content_lines = content.splitlines()
        else:
            content_lines = []
            for string in content:
                lines = string.splitlines() if string else [""]
                content_lines.extend(lines)
        for line in content_lines:
            self.result.append(f"{indent}{line.rstrip()}", sourcename, lineno)


class ConstituentDocumenter:
    titles_allowed = True
    """
    True if the generated content may contain titles.
    """

    option_spec: ClassVar[OptionSpec] = {"noindex": directives.flag}
    """
    Option specification used when generating the directive.
    """

    def __init__(
        self,
        bridge: DocumenterBridge,
        documented_object_identifier: ObjectIdentifier,
        options: Options,
        indent: str = "",
    ):
        """
        Know how to document an objtype.

        The right documenter is picked by the caller and is injected with
        what to document in what context.

        Notes:
            Subclasses **should not** be registered using the
            :meth:`sphinx.application.Sphinx.add_autodocumenter` method.
            The Autodoc documenter are only meant for documenting Python
            code, cannot be bound to a specific domain and are generally
            incompatible for general use-cases like the one we are try
            to address here.

        Args:
            bridge:
                The Sphinx application context and auto directive parameters.
            documented_object_identifier:
                The documented object itself. Any object you implement that
                you can use in your own subclasses can do.
            options:
                Directive option to add to the constituent directive.
            indent:
                Nested constituent are documented recursively with
                this increasing indentation.
        """
        self.bridge = bridge
        self.env: BuildEnvironment = bridge.env
        self.options = options
        self.indent = indent
        self.documented_object_identifier = documented_object_identifier

    def generate(
        self,
        more_content: StringList | None = None,
    ) -> None:
        """
        Generate reST for the object given by *self.name*.

        The generated content is added to the directive through the
        bridge object.

        Notes:
            This method is the only one exposed to external code. It is
            designed as a template method, which means it calls other
            methods that are to be implemented in subclasses.

        Args:
            more_content:
                When given, this content is also included.
        """
        # generate the directive header and options, if applicable
        self.add_directive_header()
        # add an empty line before the content
        self.bridge.yield_content("", self.documented_object_identifier)

        # push the current documented constituent in the context stack
        self.env.ref_context[
            f"{self.documented_object_identifier.domain_name}:scope"
        ] = self.documented_object_identifier

        # TODO(cblegare):  # noqa: FIX002
        # https://gitlab.com/fjordlynn/pf2srd/-/issues/5
        # Maybe emit and event for processing content?

        # add all content (from docstrings, attribute docs etc.)
        with self.bridge.indent():
            self.generate_content()
            self.bridge.yield_content("", self.documented_object_identifier)

            if more_content:
                for line, src in zip(more_content.data, more_content.items):
                    sourcename, lineno = src
                    self.bridge.yield_content(
                        line, sourcename=sourcename, lineno=lineno
                    )

        # add an empty line before the child constituents
        self.bridge.yield_content("", self.documented_object_identifier)

    def format_options(self) -> dict[str, str]:
        """
        Prepare the documenter options to be rendered as directive options.

        Notes:
            The need to overwrite this property in subclasses is likely.

        Returns:
            Option names to option values.
            Falsy option value means no value (a flag).
        """
        return {}

    def add_directive_header(self) -> None:
        """
        Add the directive header.

        We will add all the names as the directive arguments.
        If we have the names ["a", "b"] for a domain "d", objtype "o" and
        options {"k": "v"},  we would get the following directive header.

        .. code-block:: restructuredtext

            .. d:o:: a
                     b
                :k: v

        Notes:
            The need to overwrite this method in subclasses is unlikely.
            Subclass should overwrite :meth:`~get_signatures` or
            :meth:`~format_options` instead.
        """
        signatures = self.get_signatures()

        try:
            _ = signatures[0]
        except IndexError as exc:
            msg = "get_signature should provide at least one signature."
            raise ValueError(msg) from exc

        domain_name = self.domain_name
        directive_name = self.directive_name

        # The first signature follows the directive name
        first_signature_prefix = f".. {domain_name}:{directive_name}:: "
        signature_prefix = first_signature_prefix

        for i, name in enumerate(signatures):
            self.bridge.yield_content(
                f"{signature_prefix}{name}", object_id=self.documented_object_identifier
            )
            if i == 0:
                # Additional signatures are simply indented
                signature_prefix = " " * len(first_signature_prefix)

        with self.bridge.indent():
            for option_name, value in self.format_options().items():
                formated_value = f" {value}" if value else ""
                self.bridge.yield_content(
                    f":{option_name}:{formated_value}",
                    object_id=self.documented_object_identifier,
                )

    @property
    def domain(self) -> Domain:
        """
        Provide the domain instance.

        For specific corner cases, see :meth:`~domain_name`.

        Notes:
            The need to overwrite this property in subclasses is unlikely.

        Returns:
            The Compendia domain.

        Raises:
            KeyError: When no domain can be found.
        """
        domain = self.env.get_domain(self.domain_name)
        if not isinstance(domain, Domain):
            raise TypeError(domain)
        return domain

    @property
    def domain_name(self) -> str:
        """
        Guess the domain name of this directive.

        There are three cases:

        * The domain name is explicitly in the directive name
        * The domain name is implicitly the current default domain
        * This directive is not within a Sphinx domain.

        Compendia only works with domains, so the third case raises an error.

        Notes:
            The need to overwrite this property in subclasses is unlikely.

        Returns:
            The domain name found.

        Raises:
            KeyError: When no domain can be found.
        """
        return self.documented_object_identifier.domain_name

    @property
    def directive_name(self) -> str:
        """
        Provide the directive name, without the domain part.

        This could be different from the directive's objtype.

        Notes:
            The need to overwrite this property in subclasses is unlikely.

        Returns:
            This directive name.
        """
        return self.documented_object_identifier.objtype

    def get_signatures(self) -> tuple[str, ...]:
        """
        Provide an object signatures.

        The signatures are usually the names (unique identifiers for a given
        objtype in a given domain), with the first being the primary name
        and the others are additional names.

        Notes:
            The need to overwrite this property in subclasses is likely.

        Returns:
            At least one signature
        """
        return (self.documented_object_identifier.name,)

    def generate_content(self) -> None:
        """
        Generate the documentation content for the documented object.

        By default, no content is generated.

        Notes:
            The need to overwrite this property in subclasses is likely.
        """
        raise NotImplementedError


class AutodocDirective(SphinxDirective):
    """
    A directive class for all autodoc directives.

    It works as a dispatcher of Documenters.
    It invokes a Documenter upon running. After the processing,
    it parses and returns the content generated by Documenters.

    Notes:
        This implementation is based on
        ``AutodocDirective`` for Python. Since the
        Autodoc extension is strongly coupled around documenting Python code,
        We try to mimic its behavior, but we don't reuse any code.

        As with any directives, a few attributes are set by the constructor
        and available, like its name, arguments, options and content.
    """

    option_spec = DummyOptionSpec()
    has_content = True
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = True

    documenter_bridge_class: type[DocumenterBridge]

    def run(self) -> list[Node]:
        """
        Execute the directive.

        Notes:
            As the only directive entry point, this is implemented as a
            template method so only other smaller methods needs to be
            implemented in subclasses.

        Returns:
            The documentation nodes generated by this directive.
        """
        source, lineno = self._get_source_and_line()
        log.debug("[autodoc] %s:%s: input:\n%s", source, lineno, self.block_text)

        # resolve the documented object
        documented_object_id = self.get_documented_object_identifier()

        documenter_bridge = self.documenter_bridge_class(
            self.env,
            self.state,
            self.state.document.reporter,
            self.options,  # type: ignore[arg-type] # Options need work
        )

        try:
            # generate the output
            documenter_bridge.generate(documented_object_id, more_content=self.content)
        except Exception:
            log.warning(
                "[autodoc] An error occured while running %s in %s:%s.",
                self.directive_name,
                source,
                lineno,
            )
            log.exception("[autodoc] something went not well")

        if not documenter_bridge.result:
            return []

        log.debug("[autodoc] output:\n  %s", "\n  ".join(documenter_bridge.result))

        return self.parse_generated_content(self.state, documenter_bridge.result)

    @property
    def domain_name(self) -> str:
        """
        Guess the domain name of this directive.

        There are three cases:

        * The domain name is explicitly in the directive name
        * The domain name is implicitly the current default domain
        * This directive is not within a Sphinx domain.

        Compendia only works with domains, so the third case raises an error.

        Notes:
            The need to overwrite this property in subclasses is unlikely.

        Returns:
            The domain name found.

        Raises:
            KeyError: When no domain can be found.
        """
        name = str(self.name)
        if ":" in name:
            # Domain explicitely in the directive header
            domain_name, directive_name = name.split(":", 1)
            return domain_name

        # Domain implicitly the current default domain
        default_domain = self.env.temp_data.get("default_domain")
        domain_name = default_domain.name if default_domain else None
        domain_directives = self.env.app.registry.domain_directives.get(domain_name, {})
        if domain_directives[name] == self.__class__:
            # A directive in the default domain matches the class,
            # which is reliable enough
            return domain_name

        raise KeyError(name)

    @property
    def domain(self) -> Domain:
        """
        Provide the domain instance.

        For specific corner cases, see :meth:`~domain_name`.

        Notes:
            The need to overwrite this property in subclasses is unlikely.

        Returns:
            The Compendia domain.

        Raises:
            KeyError: When no domain can be found.
        """
        domain = self.env.get_domain(self.domain_name)
        if not isinstance(domain, Domain):
            raise TypeError(domain)
        return domain

    @property
    def directive_name(self) -> str:
        """
        Provide the directive name, without the domain part.

        This could be different from the directive's objtype.

        Notes:
            The need to overwrite this property in subclasses is unlikely.

        Returns:
            This directive name.
        """
        name = str(self.name)
        if ":" in name:
            _, directive_name = name.split(":", 1)
            return directive_name

        return name

    def get_documented_object_identifier(self) -> ObjectIdentifier:
        """
        Get the currently documented constituent.

        The default behavior is to identify the object with the directive's
        domain as the object's domain, the directive's name as the object's
        objtype and the directive's first argument as the object name.

        Returns:
            Enough to identify a single documented object.
        """
        return ObjectIdentifier(
            self.domain_name, self.directive_name[4:], self.arguments[0]
        )

    def _get_source_and_line(self) -> tuple[str | None, int | None]:
        reporter = self.state.document.reporter
        try:
            source, lineno = reporter.get_source_and_line(self.lineno)  # type: ignore[attr-defined]
        except AttributeError:
            source, lineno = (None, None)
        return source, lineno

    def parse_generated_content(
        self,
        state: RSTState,
        content: StringList,
        /,
        titles_allowed: bool = True,  # noqa: FBT001,FBT002
    ) -> list[Node]:
        """Parse an item of content generated by Documenter."""
        with switch_source_input(state, content):
            if titles_allowed:
                node: Element = nodes.section()
                # necessary so that the child nodes get the right source/line set
                node.document = state.document
                nested_parse_with_titles(state, content, node)
            else:
                node = nodes.paragraph()
                node.document = state.document
                state.nested_parse(content, 0, node)

            return node.children
