from __future__ import annotations

from typing import Iterator, Sequence

from pf2.sphinx import CONTENT_INDENT


class RowsAndHeadersHaveDifferentLenghtError(ValueError):
    def __init__(self, *lenghts: int):
        msg = (
            f"One or more rows do not have the headers length: "
            f"{', '.join(str(length) for length in lenghts)}."
        )
        super().__init__(msg)


def generate_rubric(
    content: str,
    title: str,
    *,
    content_intent: str = CONTENT_INDENT,
    make_footer: bool = True,
) -> Iterator[str]:
    """
    Generate a RestructuredText rubric.

    Args:
        content:
            A rubric do not have a content per se, it is usually used
            for giving a title to some paragraph.
        title:
            The heading
        content_intent:
            All RestructuredText generators have this indentation space
            configuration.
        make_footer:
            Whether to also generate a directive footer.

    Yields:
        Lines of RestructuredText.
    """
    yield f".. rubric:: {title}"
    if content:
        yield ""
        yield from content.splitlines()
    yield ""

    if make_footer:
        yield from generate_directive_footer(title, content_indent=content_intent)


def generate_table(  # noqa: PLR0913
    headers: Sequence[str],
    values: Sequence[Sequence[str]],
    title: str | None = None,
    options: dict[str, str] | None = None,
    *,
    content_indent: str = CONTENT_INDENT,
    make_footer: bool = True,
) -> Iterator[str]:
    """
    Generate a RestructuredText list table.

    Args:
        headers:
            Table headers (the first line)
        values:
            Table rows of values
        title:
            An optional title for the table.
        options:
            Additional options for the table.

            Defaults are:

            *   ``:header-rows: 1``
            *   ``:widths: auto``
            *   ``:class: datatable``
        content_indent:
            All RestructuredText generators have this indentation space
            configuration.
        make_footer:
            Whether to also generate a directive footer.

    Yields:
        Lines of RestructuredText.
    """
    if any(len(row) != len(headers) for row in values):
        raise RowsAndHeadersHaveDifferentLenghtError(
            len(headers), *[len(row) for row in values]
        )

    if not headers or not values:
        return ()

    options = options or {}
    options.setdefault("header-rows", "1")
    options.setdefault("widths", "auto")
    options.setdefault("class", "datatable")

    yield from _generate_table(
        table=[headers, *values],
        title=title,
        options=options,
        content_indent=content_indent,
        make_footer=make_footer,
    )


def generate_datafields(  # noqa: PLR0913
    fields: Sequence[str],
    values: Sequence[str],
    title: str | None = None,
    options: dict[str, str] | None = None,
    *,
    content_indent: str = CONTENT_INDENT,
    make_footer: bool = True,
) -> Iterator[str]:
    """
    Generate a RestructuredText list table with one stub column.

    Args:
        fields:
            Keys (the stub column)
        values:
            Values (the other column)
        title:
            An optional title for the table.
        options:
            Additional options for the table.

            Defaults are:

            *   ``:stub-columns: 1``
            *   ``:widths: auto``
            *   ``:header-rows: 0``
        content_indent:
            All RestructuredText generators have this indentation space
            configuration.
        make_footer:
            Whether to also generate a directive footer.

    Yields:
        Lines of RestructuredText.
    """
    if not fields or not values:
        return ()
    options = options or {}

    options.setdefault("stub-columns", "1")
    options.setdefault("widths", "30 70")
    options.setdefault("header-rows", "0")

    if len(fields) != len(values):
        raise RowsAndHeadersHaveDifferentLenghtError(len(fields), len(values))

    yield from _generate_table(
        table=list(zip(fields, values)),
        title=title,
        options=options,
        content_indent=content_indent,
        make_footer=make_footer,
    )


def _generate_table(  # noqa: PLR0913
    table: Sequence[Sequence[str]],
    title: str | None = None,
    footer_comment: str = "",
    options: dict[str, str] | None = None,
    *,
    content_indent: str = CONTENT_INDENT,
    make_footer: bool = True,
) -> Iterator[str]:
    yield from _generate_list_header(
        title=title, options=options, content_indent=content_indent
    )
    yield from _generate_list_content(table=table, content_indent=content_indent)
    if make_footer:
        yield from generate_directive_footer(
            title="table", comment=footer_comment, content_indent=content_indent
        )


def _generate_list_header(
    title: str | None = None,
    options: dict[str, str] | None = None,
    *,
    content_indent: str = CONTENT_INDENT,
) -> Iterator[str]:
    options = options or {}
    yield f".. list-table:: {title}" if title else ".. list-table::"
    for name, value in options.items():
        yield f"{content_indent}:{name}: {value}"
    yield ""


def _generate_list_content(
    table: Sequence[Sequence[str]],
    *,
    content_indent: str = CONTENT_INDENT,
) -> Iterator[str]:
    lenghts = {len(i) for i in table}
    if not len(lenghts) == 1:
        raise RowsAndHeadersHaveDifferentLenghtError(*[len(i) for i in table])

    for row in table:
        yield f"{content_indent}*"
        yield ""
        first_line_prefix = f"{content_indent * 2}*{' ' * (len(CONTENT_INDENT) - 1)}"
        next_line_prefix = f"{content_indent * 2}{' ' * len(CONTENT_INDENT)}"
        line_prefix = first_line_prefix
        for cell in row:
            markup_lines = str(cell).splitlines() if cell else [""]
            for line in markup_lines:
                yield f"{line_prefix}{line}"
                line_prefix = next_line_prefix
            line_prefix = first_line_prefix
            yield ""


def generate_directive_footer(
    title: str, comment: str = "", *, content_indent: str = CONTENT_INDENT
) -> Iterator[str]:
    yield f".. end of {title}"
    if comment:
        yield ".."
        for line in comment.splitlines():
            yield f"{content_indent}{line}"
    yield ""
