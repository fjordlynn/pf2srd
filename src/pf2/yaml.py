from __future__ import annotations

from typing import TYPE_CHECKING, Callable, Protocol

import yaml as pyyaml

BLOCK_STRING_THRESHOLD = 55

if TYPE_CHECKING:
    from pathlib import Path
    from typing import Sequence, Union, overload

    class JsonArray(Protocol):
        def __getitem__(self, idx: int) -> JsonLike:
            ...

        def sort(self) -> None:
            ...

    class JsonDict(Protocol):
        def __getitem__(self, key: str) -> JsonLike:
            ...

        @staticmethod
        @overload
        def fromkeys(seq: Sequence[str]) -> JsonDict:
            ...

        @staticmethod
        @overload
        def fromkeys(seq: Sequence[str], value: JsonLike) -> JsonDict:
            ...

    JsonLike = Union[str, int, float, bool, None, JsonArray, JsonDict]


class Yaml(Protocol):
    def load(self, stream: Path) -> JsonLike:
        ...

    def dumps(self, data: JsonLike) -> str:
        ...

    def _make_scalar_node(self, tag, value, start_mark=None, end_mark=None, style=None):  # type: ignore[no-untyped-def] # noqa: ANN001, ANN202, PLR0913
        ...


def make_yaml(implementation: str | Yaml = "pyyaml") -> Yaml:
    if not isinstance(implementation, str):
        return implementation
    if implementation != "pyyaml":
        raise NotImplementedError
    choices = {
        "pyyaml": _PyYaml,
        # "ruamel": _RuamelYaml
    }
    return choices[implementation]()


def make_yaml_dump(implementation: str = "pyyaml") -> Callable[[JsonLike], str]:
    return make_yaml(implementation).dumps


def make_yaml_load(implementation: str = "pyyaml") -> Callable[[Path], JsonLike]:
    return make_yaml(implementation).load


def _should_use_block(value: str) -> bool:
    as_str = str(value)
    for c in "\u000a\u000d\u001c\u001d\u001e\u0085\u2028\u2029":
        if c in as_str:
            return True
    if len(as_str) > BLOCK_STRING_THRESHOLD:
        return True
    if len(as_str.splitlines()) > 1:
        return True
    return False


def _make_str_representer(yaml_impl: Yaml):  # type: ignore[no-untyped-def] # noqa: ANN202
    def represent_str(dumper, value):  # type: ignore[no-untyped-def] # noqa: ANN001, ANN202
        style = ">" if _should_use_block(value) else dumper.default_style

        node = yaml_impl._make_scalar_node(  # type: ignore[no-untyped-call] # noqa: SLF001
            "tag:yaml.org,2002:str",
            str(value),
            style=style,
        )
        if dumper.alias_key is not None:
            dumper.represented_objects[dumper.alias_key] = node
        return node

    return represent_str


def represent_set_as_list(dumper, data):  # type: ignore[no-untyped-def] # noqa: ANN001, ANN201
    return dumper.represent_sequence("tag:yaml.org,2002:seq", list(data))


class _PyYaml(Yaml):
    class MyDumper(pyyaml.SafeDumper):
        def increase_indent(self, flow=False, _=False):  # type: ignore[no-untyped-def] # noqa: ANN001, ANN202, FBT002
            return super().increase_indent(flow, False)  # noqa: FBT003

        def ignore_aliases(self, data):  # type: ignore[no-untyped-def] # noqa: ANN001, ANN202, ARG002
            return True

    class MyLoader(pyyaml.SafeLoader):
        def construct_mapping(self, node, deep=False):  # type: ignore[no-untyped-def] # noqa: ANN001, ANN202, FBT002
            mapping = super().construct_mapping(node, deep=deep)
            # Add 1 so line numbering starts at 1
            mapping["__line_start__"] = node.start_mark.line
            mapping["__line_end__"] = node.end_mark.line
            mapping["__sourcefile__"] = self.name
            return mapping

    def __init__(self) -> None:
        self._impl = pyyaml
        self._dumper = self.MyDumper
        self._loader = self.MyLoader

        self._dumper.add_representer(set, represent_set_as_list)
        self._dumper.add_representer(str, _make_str_representer(self))
        self._dumper.add_multi_representer(str, _make_str_representer(self))
        self._dumper.add_representer(None, _make_str_representer(self))  # type: ignore[arg-type]
        self._dumper.add_multi_representer(None, _make_str_representer(self))  # type: ignore[arg-type]

    def _make_scalar_node(self, tag, value, start_mark=None, end_mark=None, style=None):  # type: ignore[no-untyped-def] # noqa: PLR0913, ANN202, ANN001
        return self._impl.representer.ScalarNode(
            tag,
            value,
            start_mark=start_mark,
            end_mark=end_mark,
            style=style,
        )

    def dumps(self, data: JsonLike) -> str:
        return self._impl.dump(
            data,
            Dumper=self._dumper,
            allow_unicode=True,
            default_flow_style=False,
            indent=2,
        )

    def load(self, stream: Path) -> JsonLike:
        with stream.open("r+t", encoding="utf-8") as opened:
            return self._impl.load(opened, Loader=self._loader)  # type: ignore[no-any-return]
