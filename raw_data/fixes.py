"""
Script pour bidouiller.

Je vais faire une structure de projet comme du monde bientôt.

En python, il n'y a pas d'accolades `{}` ni de point-virgules comme en C#.

Il faut indenter à 4 espaces.

À part quelques exceptions historiques, on utilise snake_case et
pas camelCase pour la majorité des choses.

###
        # Le reste est plus problématique, les abilités
        #
        # Toutes les abilités sont considérées comme des «activités».
        #
        # Les activités ont au minimum une «économie d'action» et un «effet»
        #
        # Ci-dessous des exemples, mails il faudrait prendre les infos de
        # monster_entry
        some_activity = {
            "name": "Tout à un nom",
            # Description verbeuse
            "description": "The cloud dragon flexes a wing and creates a billowing cloud of mist.",
            # One of '1', '2', '3', 'F', 'R', 'V', '1m', '10m', '1h'
            # (one action..., Free, Reaction, Varies, 1 minute, ...)
            "actioncost": "R",
            # Une phrase pour un prérequis
            "requirement": "The dragon is aware of the attack and has a free wing",
            # Une phrase pour un déclencheur, habituellement pour les réactions
            "trigger": "The dragon is the target of a ranged attack",
            # Fréquence d'utilisation possible
            "frequency": "once per day",
            # Certaines activités offrent un **choix** d'effets,
            # donc c'est pluriel (voir les effets, plus bas)
            "effects": [
                # Voir plus bas
            ],
            # Certaines activités ont des propriétés supplémentaires
            # Comme les «Spell Components»
            "components": ["Material", "Verbal"],
        }

        some_effect = {
            "name": "Tout à un nom",
            # Description verbeuse
            "description": "The cloud dragon flexes a wing and creates a billowing cloud of mist.",
            # Propriétés courantes
            "range": "",
            "area": "",
            "target": "",
            "duration": "",
            "maximum_duration": "",
            # Propriétés de niveau de succès
            "critical_success": "",
            "success": "",
            "failure": "",
            "critical_failure": "",
            "saving_throw": "",
            # D'autres propriétés sont possibles
            # Comme "stages" pour les poisons
        }

"""

from __future__ import annotations

###
# Importer des libraries standards
import logging
import shutil
from datetime import datetime
from pathlib import Path
from typing import Any, Dict, Protocol, Sequence, Union, overload

###
# Importer des librairies tierces
import yaml as pyyaml

from pf2.yaml import make_yaml

###
# Quelques constantes
HERE_SCRIPT = Path(__file__)
HERE_DIRECTORY = HERE_SCRIPT.parent

log = logging.getLogger()

MONSTER_SOURCE = HERE_DIRECTORY.joinpath("monster.yaml")
MONSTER_DESTINATION = HERE_DIRECTORY.joinpath("monster_UPDATED.yaml")
ARCHIVE = HERE_DIRECTORY.joinpath("monster.yaml")

###
# Fonction principale


def main():
    log.debug("Entrée dans la fonction main()")
    # Les f-strings `f""` permettent d'interpoler des variables
    # en utilisant les accolades `{}`
    log.debug(f"  HERE_SCRIPT:    {HERE_SCRIPT}")
    log.debug(f"  HERE_DIRECTORY: {HERE_DIRECTORY}")

    yaml = make_yaml()

    kind = "weapongroup"

    source = HERE_DIRECTORY.joinpath(f"{kind}.yaml")

    raw_data = yaml.load(source)
    updated_items = {}
    updated_data = {
        "name": kind,
        "kind": "collection",
        "description": raw_data.get("description", ""),
        "items": updated_items,
    }

    for item in raw_data["items"]:
        name = item["name"]
        # for name, item in raw_data["items"].items():
        updated_items[name] = {
            "name": name,
            "kind": kind,
            "description": item.get("description", ""),
            "sources": item.get("sources", []),
            "traits": item.get("traits", []),
            "__old": item.get("__old", item),
            # "bulk": item.get("bulk"),
            # "amount": item.get("amount", 1),
            # "price": item.get("price_cp", 0.0) / 100,
            # "hands": item.get("hands", 0),
            # "level": item.get("level", 0),
            # "itemcategory": item.get("itemcategory"),
            # "staffspells": item.get("staffspells", []),
            # "spells": item.get("spells", []),
            # "progression": item.get("progression", []),
            # "type": item.get("type"),
            # "level": item.get("level"),
            # "traditions": item.get("traditions", []),
            # "heightened": item.get("heightened", []),
            # "components": item.get("components", []),
            # "activities": item.get("activities", []),
            # "has_been_manually_proofread": item.get("has_been_manually_proofread", False),
            # "boost_choices": item.get("ability_boost_choices"),
            # "is_adventure_path_specific": item.get("is_specific_to_adv"),
            # "boosts": item.get("boosts", []),
            # "flaws": item.get("flaws", []),
            # "hp": item.get("hp"),
            # "senses": item.get("senses"),
            # "size": item.get("size"),
            # "speed": item.get("speed"),
            # "flavor_text": item.get("flavor_text", ""),
            # "activities": item["__old"].get("activities"),
            # "has_been_manually_proofread": item.get("has_been_manually_proofread"),
            # "level": item.get("level", 0),
            # "prerequisites": item.get("prerequisites", []),
            # "requirement": item.get("requirement"),
            # "reach_long": item["__old"].get("reach_long_ft"),
            # "reach_tall": item["__old"].get("reach_tall_ft"),
            # "space": item["__old"].get("space_in_ft"),
            # "requirement": item.get("requirement"),
            # "trigger": item["trigger"],
            # "abbreviation": item["abbr"],
            # "damagecategory": item["damagecategory"],
            # "effects": item["effects"],
        }
    make_source_backup(source)
    source.write_text(yaml.dumps(updated_data))


def make_source_backup(source_file: Path) -> Path:
    backup_file = source_file.parent.joinpath(
        f"backup/{source_file.stem}-{datetime.now().isoformat()}.yaml"
    )
    log.debug(f"  BACKING UP: {source_file} -> {backup_file}")
    shutil.copy(source_file, backup_file)


###
# Fonctions utilitaire, probablement pas besoin d'y toucher.


def logging_configuration(verbosity: int) -> Dict[str, Any]:
    """
    Build logging configuration based on a verbosity level.

    Args:
        verbosity: Verbosity 0 is means "CRITICAL", and each
            increments move toward "DEBUG".
    """
    level = logging.CRITICAL - logging.DEBUG * verbosity

    if level < logging.DEBUG:
        format = "%(levelname)8s %(name)-24s> %(message)s"
    else:
        format = "%(levelname)8s: %(message)s"

    return dict(
        level=level,
        format=format,
    )


###
# Fonctions pour personnaliser notre manière de lire et écrire le YAML.


class _JSONArray(Protocol):
    def __getitem__(self, idx: int) -> "JSONLike":
        ...

    # hack to enforce an actual list
    def sort(self) -> None:
        ...


class _JSONDict(Protocol):
    def __getitem__(self, key: str) -> "JSONLike":
        ...

    # hack to enforce an actual dict
    @staticmethod
    @overload
    def fromkeys(seq: Sequence[Any]) -> Dict[Any, Any]:
        ...

    @staticmethod
    @overload
    def fromkeys(seq: Sequence[Any], value: Any) -> Dict[Any, Any]:
        ...


JSONLike = Union[str, int, float, bool, None, _JSONArray, _JSONDict]


class Yaml(Protocol):
    def load(self, stream: Union[Path, Any]) -> JSONLike:
        ...

    def dumps(self, data: Any) -> str:
        ...

    def _make_scalar_node(self, tag, value, start_mark=None, end_mark=None, style=None):
        ...


def _should_use_block(value):
    as_str = str(value)
    for c in "\u000a\u000d\u001c\u001d\u001e\u0085\u2028\u2029":
        if c in as_str:
            return True
    if len(as_str) > 55:
        return True
    return False


def _make_str_representer(yaml_impl: Yaml):
    def represent_str(dumper, value):
        if _should_use_block(value):
            style = ">"
        else:
            style = dumper.default_style

        node = yaml_impl._make_scalar_node(
            "tag:yaml.org,2002:str", str(value), style=style
        )
        if dumper.alias_key is not None:
            dumper.represented_objects[dumper.alias_key] = node
        return node

    return represent_str


def represent_set_as_list(dumper, data):
    return dumper.represent_sequence("tag:yaml.org,2002:seq", list(data))


class _PyYaml(Yaml):
    class MyDumper(pyyaml.SafeDumper):
        def increase_indent(self, flow=False, indentless=False):
            return super().increase_indent(flow, False)

        def ignore_aliases(self, data):
            return True

    def __init__(self):
        import yaml

        self._impl = yaml
        self._dumper = self.MyDumper

        self._dumper.add_representer(set, represent_set_as_list)
        self._dumper.add_representer(str, _make_str_representer(self))
        self._dumper.add_multi_representer(str, _make_str_representer(self))
        self._dumper.add_representer(None, _make_str_representer(self))
        self._dumper.add_multi_representer(None, _make_str_representer(self))

    def _make_scalar_node(self, tag, value, start_mark=None, end_mark=None, style=None):
        return self._impl.representer.ScalarNode(
            tag,
            value,
            start_mark=start_mark,
            end_mark=end_mark,
            style=style,
        )

    def dumps(self, data: Any) -> str:
        return self._impl.dump(
            data,
            Dumper=self._dumper,
            allow_unicode=True,
            default_flow_style=False,
            indent=2,
            # width=72,
        )

    def load(self, stream: Path) -> JSONLike:
        """
        A safe YAML loader.
        """
        return self._impl.safe_load(stream.read_text())  # type: ignore


if __name__ == "__main__":
    # Point d'entrée principal, qui invoque la fonction main()

    quiet = 0
    verbose = 3

    verbosity: int = int(logging.INFO / 10) + verbose - quiet
    logging_config = logging_configuration(verbosity)
    logging.basicConfig(**logging_config)

    main()
