items:
  Hellcrown:
    __old:
      ability_mods:
        cha_mod: 1
        con_mod: 3
        dex_mod: 4
        int_mod: -1
        str_mod: -2
        wis_mod: 1
      ac: 16
      ac_special: null
      active_abilities:
        - actioncost: None
          critical_failure: null
          critical_success: null
          description: >-
            If the hellcrown hits with a nail Strike, the target must attempt a DC
            16 Fortitude save. On a failure, the nail is embedded in the creature,
            making it __enfeebled 1__ for 1 minute and giving it 1 persistent bleed
            damage (__enfeebled 2__ and 1d4 persistent bleed damage on a critical
            failure). Each additional embedded nail increases the __enfeebled__ value
            by 1 (to a maximum of __enfeebled 4__) and the bleed damage by 1. A creature
            can remove a nail with an Interact action to reduce the __enfeeblement__
            and amount of bleed damage. Pulling out the last nail removes both conditions.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                If the hellcrown hits with a nail Strike, the target must attempt
                a DC 16 Fortitude save. On a failure, the nail is embedded in the
                creature, making it __enfeebled 1__ for 1 minute and giving it 1 persistent
                bleed damage (__enfeebled 2__ and 1d4 persistent bleed damage on a
                critical failure). Each additional embedded nail increases the __enfeebled__
                value by 1 (to a maximum of __enfeebled 4__) and the bleed damage
                by 1. A creature can remove a nail with an Interact action to reduce
                the __enfeeblement__ and amount of bleed damage. Pulling out the last
                nail removes both conditions.
              duration: null
              failure: null
              name: Bleeding Nail
              range: null
              sources:
                - abbr: 'Pathfinder #145: Hellknight Hill'
                  page_start: '90'
                  page_stop: null
              success: null
              target: null
              traits: null
          failure: null
          frequency: null
          maximum_duration: null
          name: Bleeding Nail
          range: null
          raw_description: >-
            **Bleeding Nail** If the hellcrown hits with a nail Strike, the target
            must attempt a DC 16 Fortitude save. On a failure, the nail is embedded
            in the creature, making it __enfeebled 1__ for 1 minute and giving it
            1 persistent bleed damage (__enfeebled 2__ and 1d4 persistent bleed damage
            on a critical failure). Each additional embedded nail increases the __enfeebled__
            value by 1 (to a maximum of __enfeebled 4__) and the bleed damage by 1.
            A creature can remove a nail with an Interact action to reduce the __enfeeblement__
            and amount of bleed damage. Pulling out the last nail removes both conditions.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits: null
          trigger: null
        - actioncost: One Action
          critical_failure: null
          critical_success: null
          description: >-
            All creatures that can see the hellcrown and are suffering from its bleeding
            nail must attempt a DC 16 Will saving throw. A creature that fails is
            __frightened 1__, and on a critical failure becomes fleeing for as long
            as it's __frightened__. Any creature that attempts a save is then temporarily
            immune for 10 minutes.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                All creatures that can see the hellcrown and are suffering from its
                bleeding nail must attempt a DC 16 Will saving throw. A creature that
                fails is __frightened 1__, and on a critical failure becomes fleeing
                for as long as it's __frightened__. Any creature that attempts a save
                is then temporarily immune for 10 minutes.
              duration: null
              failure: null
              name: Terrifying Stare
              range: null
              sources:
                - abbr: 'Pathfinder #145: Hellknight Hill'
                  page_start: '90'
                  page_stop: null
              success: null
              target: null
              traits:
                - fear
                - mental
                - visual
          failure: null
          frequency: null
          maximum_duration: null
          name: Terrifying Stare
          range: null
          raw_description: >-
            **Terrifying Stare** [One Action]  (__fear__, __mental__, __visual__)
            All creatures that can see the hellcrown and are suffering from its bleeding
            nail must attempt a DC 16 Will saving throw. A creature that fails is
            __frightened 1__, and on a critical failure becomes fleeing for as long
            as it's __frightened__. Any creature that attempts a save is then temporarily
            immune for 10 minutes.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits:
            - fear
            - mental
            - visual
          trigger: null
      alignment: LE
      description: >-
        The life of a Hellknight is bloody, brutal, and often short. Many who perish
        in service to a Hellknight order are glad to rest after having served their
        masters so faithfully, but others seek to continue their work even in death.
        When a Hellknight is decapitated, the ghostly undead known as a hellcrown
        is an occasional result. Consumed by the desire to bring about order by inflicting
        cruelty, hellcrowns haunt battlefields and abandoned castles, slaying all
        they encounter. A strange fusion of spirit and steel, a hellcrown has no corporeal
        form by itself, but instead inhabits the helmet it so proudly wore in life.
        Dangling from the helmet like a shroud is a shadow of the former Hellknight's
        spine, adding to the creature's terrifyingly gruesome appearance. Hellknights
        regard hellcrowns with a mixture of disgust and respect, considering the individuals
        who transform into these floating undead to have been resolute in purpose
        but weak in body.

        A hellcrown usually manifests hours or longer after its body's death, and
        only when its body is unattended; this might even take days if the Hellknight
        perished in a major battle. Typically, no recognizable fragment of the Hellknight's
        former personality survives the grisly transformation into a hellcrown, but
        in rare and particularly tragic cases a hellcrown might remember its life
        and hold grudges against those it views as the cause of its death. Regardless
        of whether they retain memories of their lives or have lost all former sense
        of self, hellcrowns linger around the site of their death, reminding all they
        encounter of the merciless principles of their order.
      hp: 20
      hp_misc: negative healing
      immunities:
        - death effects
        - disease
        - paralyzed
        - poison
        - unconscious
      languages:
        - Common
      level: 1
      melee_attacks: null
      name: Hellcrown
      old_recall_knowledge:
        - dc: 17
          skill: ' - Undead__ (__Religion__)'
      perception: 10
      ranged_attacks:
        - actioncost: One Action
          damage:
            - formula: 1d4+2
              type: piercing
            - formula: null
              type: bleeding nail
          name: nail
          to_hit: 9
          traits:
            - range increment 20 feet
      rarity: Uncommon
      recall_knowledge:
        - dc: 17
          skill: Religion
          trait: Undead
      saves:
        fort: 4
        fort_misc: null
        misc: null
        ref: 6
        ref_misc: null
        will: 10
        will_misc: null
      senses:
        - darkvision
      size: Tiny
      skills:
        - bonus: 8
          misc: null
          name: Intimidation
        - bonus: 7
          misc: null
          name: Stealth
      sources:
        - abbr: 'Pathfinder #145: Hellknight Hill'
          page_start: '90'
          page_stop: null
      speed:
        - amount: 25
          misc: null
          type: fly
      spell_lists: null
      traits:
        - Undead
    ability_mods:
      cha_mod: 1
      con_mod: 3
      dex_mod: 4
      int_mod: -1
      str_mod: -2
      wis_mod: 1
    ac: 16
    activities:
      Bleeding Nail:
        __old:
          actioncost: None
          critical_failure: null
          critical_success: null
          description: >-
            If the hellcrown hits with a nail Strike, the target must attempt a DC
            16 Fortitude save. On a failure, the nail is embedded in the creature,
            making it __enfeebled 1__ for 1 minute and giving it 1 persistent bleed
            damage (__enfeebled 2__ and 1d4 persistent bleed damage on a critical
            failure). Each additional embedded nail increases the __enfeebled__ value
            by 1 (to a maximum of __enfeebled 4__) and the bleed damage by 1. A creature
            can remove a nail with an Interact action to reduce the __enfeeblement__
            and amount of bleed damage. Pulling out the last nail removes both conditions.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                If the hellcrown hits with a nail Strike, the target must attempt
                a DC 16 Fortitude save. On a failure, the nail is embedded in the
                creature, making it __enfeebled 1__ for 1 minute and giving it 1 persistent
                bleed damage (__enfeebled 2__ and 1d4 persistent bleed damage on a
                critical failure). Each additional embedded nail increases the __enfeebled__
                value by 1 (to a maximum of __enfeebled 4__) and the bleed damage
                by 1. A creature can remove a nail with an Interact action to reduce
                the __enfeeblement__ and amount of bleed damage. Pulling out the last
                nail removes both conditions.
              duration: null
              failure: null
              name: Bleeding Nail
              range: null
              sources:
                - abbr: 'Pathfinder #145: Hellknight Hill'
                  page_start: '90'
                  page_stop: null
              success: null
              target: null
              traits: null
          failure: null
          frequency: null
          maximum_duration: null
          name: Bleeding Nail
          range: null
          raw_description: >-
            **Bleeding Nail** If the hellcrown hits with a nail Strike, the target
            must attempt a DC 16 Fortitude save. On a failure, the nail is embedded
            in the creature, making it __enfeebled 1__ for 1 minute and giving it
            1 persistent bleed damage (__enfeebled 2__ and 1d4 persistent bleed damage
            on a critical failure). Each additional embedded nail increases the __enfeebled__
            value by 1 (to a maximum of __enfeebled 4__) and the bleed damage by 1.
            A creature can remove a nail with an Interact action to reduce the __enfeeblement__
            and amount of bleed damage. Pulling out the last nail removes both conditions.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits: null
          trigger: null
        __original_key: active_abilities
        actioncost: None
        effects:
          Bleeding Nail:
            __old:
              area: null
              critical_failure: null
              critical_success: null
              description: >-
                If the hellcrown hits with a nail Strike, the target must attempt
                a DC 16 Fortitude save. On a failure, the nail is embedded in the
                creature, making it __enfeebled 1__ for 1 minute and giving it 1 persistent
                bleed damage (__enfeebled 2__ and 1d4 persistent bleed damage on a
                critical failure). Each additional embedded nail increases the __enfeebled__
                value by 1 (to a maximum of __enfeebled 4__) and the bleed damage
                by 1. A creature can remove a nail with an Interact action to reduce
                the __enfeeblement__ and amount of bleed damage. Pulling out the last
                nail removes both conditions.
              duration: null
              failure: null
              name: Bleeding Nail
              range: null
              sources:
                - abbr: 'Pathfinder #145: Hellknight Hill'
                  page_start: '90'
                  page_stop: null
              success: null
              target: null
              traits: null
            actioncost: None
            name: Bleeding Nail
        name: Bleeding Nail
      Terrifying Stare:
        __old:
          actioncost: One Action
          critical_failure: null
          critical_success: null
          description: >-
            All creatures that can see the hellcrown and are suffering from its bleeding
            nail must attempt a DC 16 Will saving throw. A creature that fails is
            __frightened 1__, and on a critical failure becomes fleeing for as long
            as it's __frightened__. Any creature that attempts a save is then temporarily
            immune for 10 minutes.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                All creatures that can see the hellcrown and are suffering from its
                bleeding nail must attempt a DC 16 Will saving throw. A creature that
                fails is __frightened 1__, and on a critical failure becomes fleeing
                for as long as it's __frightened__. Any creature that attempts a save
                is then temporarily immune for 10 minutes.
              duration: null
              failure: null
              name: Terrifying Stare
              range: null
              sources:
                - abbr: 'Pathfinder #145: Hellknight Hill'
                  page_start: '90'
                  page_stop: null
              success: null
              target: null
              traits:
                - fear
                - mental
                - visual
          failure: null
          frequency: null
          maximum_duration: null
          name: Terrifying Stare
          range: null
          raw_description: >-
            **Terrifying Stare** [One Action]  (__fear__, __mental__, __visual__)
            All creatures that can see the hellcrown and are suffering from its bleeding
            nail must attempt a DC 16 Will saving throw. A creature that fails is
            __frightened 1__, and on a critical failure becomes fleeing for as long
            as it's __frightened__. Any creature that attempts a save is then temporarily
            immune for 10 minutes.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits:
            - fear
            - mental
            - visual
          trigger: null
        __original_key: active_abilities
        actioncost: One Action
        effects:
          Terrifying Stare:
            __old:
              area: null
              critical_failure: null
              critical_success: null
              description: >-
                All creatures that can see the hellcrown and are suffering from its
                bleeding nail must attempt a DC 16 Will saving throw. A creature that
                fails is __frightened 1__, and on a critical failure becomes fleeing
                for as long as it's __frightened__. Any creature that attempts a save
                is then temporarily immune for 10 minutes.
              duration: null
              failure: null
              name: Terrifying Stare
              range: null
              sources:
                - abbr: 'Pathfinder #145: Hellknight Hill'
                  page_start: '90'
                  page_stop: null
              success: null
              target: null
              traits:
                - fear
                - mental
                - visual
            actioncost: One Action
            name: Terrifying Stare
        name: Terrifying Stare
    alignment: LE
    description: >-
      The life of a Hellknight is bloody, brutal, and often short. Many who perish
      in service to a Hellknight order are glad to rest after having served their
      masters so faithfully, but others seek to continue their work even in death.
      When a Hellknight is decapitated, the ghostly undead known as a hellcrown is
      an occasional result. Consumed by the desire to bring about order by inflicting
      cruelty, hellcrowns haunt battlefields and abandoned castles, slaying all they
      encounter. A strange fusion of spirit and steel, a hellcrown has no corporeal
      form by itself, but instead inhabits the helmet it so proudly wore in life.
      Dangling from the helmet like a shroud is a shadow of the former Hellknight's
      spine, adding to the creature's terrifyingly gruesome appearance. Hellknights
      regard hellcrowns with a mixture of disgust and respect, considering the individuals
      who transform into these floating undead to have been resolute in purpose but
      weak in body.

      A hellcrown usually manifests hours or longer after its body's death, and only
      when its body is unattended; this might even take days if the Hellknight perished
      in a major battle. Typically, no recognizable fragment of the Hellknight's former
      personality survives the grisly transformation into a hellcrown, but in rare
      and particularly tragic cases a hellcrown might remember its life and hold grudges
      against those it views as the cause of its death. Regardless of whether they
      retain memories of their lives or have lost all former sense of self, hellcrowns
      linger around the site of their death, reminding all they encounter of the merciless
      principles of their order.
    hp: 20
    hp_misc: negative healing
    items: null
    languages:
      - Common
    level: 1
    name: Hellcrown
    perception: 10
    rarity: Uncommon
    recall_knowledge:
      - dc: 17
        skill: Religion
        trait: Undead
    resistances: null
    saves:
      fort: 4
      fort_misc: null
      misc: null
      ref: 6
      ref_misc: null
      will: 10
      will_misc: null
    senses:
      - darkvision
    size: Tiny
    skills:
      - bonus: 8
        misc: null
        name: Intimidation
      - bonus: 7
        misc: null
        name: Stealth
    sources:
      - abbr: 'Pathfinder #145: Hellknight Hill'
        page_start: '90'
        page_stop: null
    speed:
      - amount: 25
        misc: null
        type: fly
    traits:
      - Undead
