import textwrap

from pf2.sphinx.rstgen import generate_rubric


def test_rubric():
    title = "Some rubric title"
    content = "Some content"

    generated_rst = generate_rubric(
        content,
        title,
    )

    assert "\n".join(generated_rst) == textwrap.dedent(
        f"""
        .. rubric:: {title}

        {content}

        .. end of {title}
        """.strip("\n")
    )
