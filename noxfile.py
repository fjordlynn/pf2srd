from __future__ import annotations

from pathlib import Path
from typing import List

import nox

repo_root = Path(__file__).parent
build_root = repo_root / "build"
build_root.mkdir(parents=True, exist_ok=True)


nox.options.sessions = ["lint", "build", "test", "coverage"]


@nox.session(reuse_venv=True)
def build(session):
    session.install("build", "twine")
    session.run("python", "-m", "build")
    session.run("python", "-m", "twine", "check", "--strict", "dist/*")


@nox.session(python="3.11")
def test(session):
    session.install(".[markdown]", "pytest", "coverage[toml]")
    session.run(*_coverage_cmd(session.name, ["pytest", "test"]))


@nox.session(reuse_venv=True)
def requirements(session):
    session.install("pip-tools")
    session.run(
        "pip-compile",
        "--resolver=backtracking",
        "--generate-hashes",
        "--annotate",
        "--emit-index-url",
        "--upgrade",
        "--output-file",
        build_root.joinpath("requirements.txt").relative_to(repo_root),
    )


@nox.session(reuse_venv=True)
def format(session):
    session.install(
        #"black",
        "isort",
        "ruff"
    )
    #session.run("black", ".")
    session.run("isort", ".")
    session.run("ruff", "format", "src")


@nox.session(reuse_venv=True)
def lint(session):
    session.install(
        "ruff",
        #"black",
        #"isort"
    )
    #session.run("black", "--check", "--diff", "src")
    #session.run("isort", "--check", "--diff", "src")
    session.run("ruff", "check", "src", "--fix", "--unsafe-fixes")


@nox.session(reuse_venv=True)
def typing(session):
    session.install(
        ".[typing]",
        "mypy",
        "lxml",
        "docutils-stubs",
        "importlib_metadata",
    )
    session.run("mypy", "--python-version", "3.11", "src")


@nox.session(reuse_venv=True)
def coverage(session):
    session.install(".", "coverage[toml]")
    # With --keep, coverage can be run multiple times
    # without exiting with non-zero
    session.run("coverage", "combine", "--keep")
    session.run("coverage", "xml")
    session.run("coverage", "html")
    session.run("coverage", "report")


@nox.session(python="3.11", reuse_venv=True)
def docs(session):
    session.install("-e.[docs]")
    # session.run(*_python_cmd(_sphinx_apidoc_modulecmd()))
    session.run(*_python_cmd(_sphinx_build_modulecmd("build/sphinx")))


@nox.session(python="3.11", reuse_venv=True)
def pdf(session):
    session.install("-e.[docs]")
    # session.run(*_python_cmd(_sphinx_apidoc_modulecmd()))
    session.run(*_python_cmd(_sphinx_build_modulecmd("build/sphinx", "latexpdf")))


def _sphinx_apidoc_modulecmd() -> List[str]:
    return [
        "sphinx.ext.apidoc",
        "-o",
        "docs/apidoc",
        "--force",
        "--no-toc",
        "--separate",
        "--module-first",
        "src",
    ]


def _sphinx_build_modulecmd(build_root: str, builder: str = "html") -> List[str]:
    return [
        "sphinx",
        "-b",
        builder,
        "-d",
        f"{build_root}/doctrees",
        "-E",
        "-n",
        "-W",
        "--keep-going",
        "-T",
        "docs",
        f"{build_root}/html",
    ]


def _python_cmd(modulecmd: List[str]) -> List[str]:
    return ["python", "-m", *modulecmd]


def _coverage_cmd(context: str, modulecmd: List[str]) -> List[str]:
    return [
        "python",
        "-m",
        "coverage",
        "run",
        f"--context={context}",
        "-m",
        *modulecmd,
    ]
